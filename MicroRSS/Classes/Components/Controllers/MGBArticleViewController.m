//
//  MGBArticleViewController.m
//  MicroRSS
//
//  Created by Grzesiek on 06/22/13.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import "MGBArticleViewController.h"
#import "DataServiceTreeItem.h"
#import "TinyTinyRSSArticle.h"
#import "ArticleTableRowView.h"
#import "MGBArticleViewControllerDelegate.h"
#import "ArticleTableMoreRowView.h"
#import "TinyTinyRSSFeed.h"
#import "MGBArticlePreviewWindowController.h"
#import "MGBAppDelegate.h"
#import <WebKit/WebView.h>
#import <WebKit/WebKit.h>

static unsigned char staticUpdateTimeDisplayMethod = MGB_UPDATE_TIME_DISPLAY_ELAPSED_DAYS;
static NSString *staticArticleViewIdentifier = @"ArticleTableRowViewKey";

@interface MGBArticleViewController ()

// Aktualnie wybrana kategoria z której wyświetlane są artykuły
@property (nonatomic, retain) DataServiceTreeItem *category;

// Tablica z aktualnie wyświetlanymi nagłówkami
@property(nonatomic, retain) NSMutableArray *headlinesArray;

// Czy załadowano wszystkie dostępne artykuły
@property (nonatomic, assign) BOOL isAllArticlesLoaded;

// Kontroler odpowiedzialny za wyświetlanie podglądu
@property (nonatomic, retain) MGBArticlePreviewWindowController *previewWindowController;

// Wczytuje dany artykuł do podglądu
- (void)loadArticlePreview:(TinyTinyRSSArticle *)article;

// Zwraca kontroler wyświetlający podgląd tworząc go jeśli zachodzi taka potrzeba
- (MGBArticlePreviewWindowController *)createPreviewWindowController;
@end

@implementation MGBArticleViewController

// ---------------------------------------------------------------------------------------------------------------------
- (void)awakeFromNib
{
    [super awakeFromNib];

    [self.headlinesTable setTarget:self];
    [self.headlinesTable setDoubleAction:@selector(goToWebPage:)];
}


// ---------------------------------------------------------------------------------------------------------------------
- (void)setCategoryItem:(DataServiceTreeItem *)item
{
    [self.category cancelArticleDownload];

    self.category = item;
    self.headlinesArray = nil; // czyszczenie danych przed ich ponownym załadowaniem
    [self.headlinesTable reloadData];
    [self.feedIconImageView setHidden:YES];
    self.isAllArticlesLoaded = NO;

    if (item) {
        [self.loadingIndicator setHidden:NO];
        [self.loadingIndicator startAnimation:self];
        self.feedTitleLabel.stringValue = item.title;

        TinyTinyRSSFeed *feed = (TinyTinyRSSFeed *)item;
        [self.sortOrderControll setSelectedSegment:feed.articlesSortOrder == MGBSortingOrderDescending ? 0 : 1];
        [self.unreadFilterControll setSelectedSegment:feed.articlesVisibility == MGBArticleVisibilityAll ? 1 : 0];

        [item downloadArticles:^(NSMutableArray *headlines) {
            self.headlinesArray = headlines;

            [self.headlinesTable reloadData];

            [self.loadingIndicator stopAnimation:self];
            [self.loadingIndicator setHidden:YES];
            if (item.icon) {
                [self.feedIconImageView setImage:item.icon];
                [self.feedIconImageView setHidden:NO];
            }

            if (self.articleToSelect) {
                for (NSUInteger i = 0; i < headlines.count; i++) {
                    TinyTinyRSSArticle *article = [headlines objectAtIndex:i];

                    if (article.articleID == self.articleToSelect) {
                        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:i];
                        [self.headlinesTable selectRowIndexes:indexSet byExtendingSelection:NO];
                        break;
                    }
                }

                self.articleToSelect = 0;
            }
        }];
    } else {
        [self.loadingIndicator setHidden:YES];
        [self.loadingIndicator stopAnimation:self];
        self.feedTitleLabel.stringValue = @"";
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)updateArticlesIfNeeded
{
    if ([self.category isArticleDataChanged]) {
        [self.feedIconImageView setHidden:YES];
        [self.loadingIndicator setHidden:NO];
        [self.loadingIndicator startAnimation:self];
        self.isAllArticlesLoaded = NO;

        [self.category downloadArticles:^(NSMutableArray *headlines) {
            self.headlinesArray = headlines;

            [self.headlinesTable reloadData];

            [self.loadingIndicator stopAnimation:self];
            [self.loadingIndicator setHidden:YES];
            if (self.category.icon) {
                [self.feedIconImageView setImage:self.category.icon];
                [self.feedIconImageView setHidden:NO];
            }
        }];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)goToWebPage:(id)sender
{
    NSUInteger selectedRow = (NSUInteger)[self.headlinesTable selectedRow];

    TinyTinyRSSArticle *selectedHeadline = [self.headlinesArray objectAtIndex:selectedRow];

    if (selectedHeadline) {
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:selectedHeadline.link]];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)markAsUnread:(id)sender
{
    NSUInteger selectedRow = (NSUInteger)[self.headlinesTable selectedRow];

    TinyTinyRSSArticle *selectedHeadline = [self.headlinesArray objectAtIndex:selectedRow];

    if (selectedHeadline && !selectedHeadline.unread) {
        // oznaczamy jako nie przeczytane
        [selectedHeadline markAsUnread:YES];
        ArticleTableRowView *rowView = [self.headlinesTable rowViewAtRow:selectedRow makeIfNecessary:NO];
        rowView.unread = YES;
        [self.delegate feedsDataChanged:self];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (DataServiceTreeItem *)categoryItem
{
    return self.category;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)reloadArticlesList
{
    [self.headlinesTable reloadData];
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)print:(id)sender
{

    [self.webView.mainFrame.frameView.documentView print:sender];
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)changeSortOrder:(id)sender
{
    NSInteger newOrder = [self.sortOrderControll selectedSegment];

    TinyTinyRSSFeed *feed = (TinyTinyRSSFeed *)self.categoryItem;

    feed.articlesSortOrder = newOrder == 0 ? MGBSortingOrderDescending : MGBSortingOrderAscending;
    [self setCategoryItem:feed];
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)changeUnreadFilter:(id)sender
{
    NSInteger newFilter = [self.unreadFilterControll selectedSegment];

    TinyTinyRSSFeed *feed = (TinyTinyRSSFeed *)self.categoryItem;

    feed.articlesVisibility = newFilter == 1 ? MGBArticleVisibilityAll : MGBArticleVisibilityUnread;
    [self setCategoryItem:feed];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)loadArticlePreview:(TinyTinyRSSArticle *)article
{
    NSURL *linkURL = [[NSURL alloc] initWithString:article.link];

    NSMutableString *previewContent = [[NSMutableString alloc] initWithString:@"<html>"
            "<head>"
            "<style>"
            "* {font-family: \"Lucida Grande\";} "
            "body {padding: 5px;} "
            ".mrss-article-title {padding: 0 5px 2px 5px; font-size: 12pt; font-weight: bold;} "
            ".mrss-article-title a {color: black; text-decoration: none;} "
            ".mrss-article-date {padding: 0 5px 2px 5px; color: #6E6E6E; border-bottom: 1px solid #6E6E6E; margin-bottom: 5px; font-size: 9pt;}"
            ".mrss-article-content {font-size: 10pt;}"
            ".mrss-article-date-table-cell {color: #6E6E6E; font-size: 9pt;}"
            ".mrss-100proc {width: 100%;}"
            ".mrss-70proc {width: 70%;}"
            ".mrss-30proc {width: 30%;}"
            "</style>"
            "</head>"
            "<body>"];

    [previewContent appendFormat:@"<div class=\"mrss-article-title\"><a href=\"%@\" target=\"_blank\">%@</a></div>", article.link, article.articleTitle];

    NSDate *articleDate            = [[NSDate alloc] initWithTimeIntervalSince1970:(NSTimeInterval)article.updated];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"d MMMM YYYY HH:mm"];

    [previewContent appendFormat:@"<div class=\"mrss-article-date mrss-100proc\"><table class=\"mrss-100proc\"><tr>"
                                         "<td class=\"mrss-article-date-table-cell mrss-70proc\">%@</td>"
                                         "<td class=\"mrss-article-date-table-cell mrss-30proc\" style=\"text-align: right;\">%@</td></tr></table></div>"
                                         "<div class=\"mrss-article-content\">", article.feed.name, [dateFormatter stringFromDate:articleDate]];

    [previewContent appendString:article.content];

    [previewContent appendString:@"</div></body></html>"];

    [self.webView.mainFrame loadHTMLString:previewContent baseURL:linkURL];
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark - ArticleTableRowViewDelegate
- (void)markArticleForRow:(NSInteger)row starred:(BOOL)starred
{
    TinyTinyRSSArticle *selectedArticle = [self.headlinesArray objectAtIndex:(NSUInteger)row];

    [selectedArticle markAsStarred:starred];

    [self.delegate feedsDataChanged:self];

    if ([self.delegate respondsToSelector:@selector(feedStarSelectionChanged:)]) {
        [self.delegate feedStarSelectionChanged:starred];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)markArticleForRow:(NSInteger)row published:(BOOL)published
{
    TinyTinyRSSArticle *selectedArticle = [self.headlinesArray objectAtIndex:(NSUInteger)row];

    [selectedArticle markAsPublished:published];

    [self.delegate feedsDataChanged:self];

    if ([self.delegate respondsToSelector:@selector(feedPublishSelectionChanged:)]) {
        [self.delegate feedPublishSelectionChanged:published];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)showArticlePreviewForRow:(NSInteger)row
{
    TinyTinyRSSArticle *selectedArticle = [self.headlinesArray objectAtIndex:(NSUInteger)row];

    MGBArticlePreviewWindowController *previewController = [self createPreviewWindowController];

    MGBAppDelegate *appDelegate = (MGBAppDelegate *) [[NSApplication sharedApplication] delegate];
    NSRect mainWindowFrame      = appDelegate.window.frame;

    CGFloat widthDiff = mainWindowFrame.size.width - previewController.window.frame.size.width;

    NSRect previewWindowFrame = NSMakeRect(mainWindowFrame.origin.x + widthDiff / 2, mainWindowFrame.origin.y, previewController.window.frame.size.width, previewController.window.frame.size.height);
    [previewController.window setFrame:previewWindowFrame display:YES];

    [previewController.window makeKeyAndOrderFront:self];

    [previewController loadPreviewForURL:selectedArticle.link];
}

// ---------------------------------------------------------------------------------------------------------------------
- (MGBArticlePreviewWindowController *)createPreviewWindowController
{
    if (!self.previewWindowController) {
        self.previewWindowController = [[MGBArticlePreviewWindowController alloc] initWithWindowNibName:@"MGBArticlePreviewWindow"];
    }

    return self.previewWindowController;
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark - NSTableViewDataSource
- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    if (self.headlinesArray.count > 0 && self.headlinesArray.count % 50 == 0 && !self.isAllArticlesLoaded) {
        return self.headlinesArray.count + 1;
    } else {
        return self.headlinesArray.count;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark - NSTableViewDelegate
- (void)tableViewSelectionDidChange:(NSNotification *)notification
{
    NSInteger selectedRow = [self.headlinesTable selectedRow];

    if (selectedRow > -1) {
        if (selectedRow < self.headlinesArray.count) {
            TinyTinyRSSArticle *selectedArticle = [self.headlinesArray objectAtIndex:(NSUInteger)selectedRow];

            [self loadArticlePreview:selectedArticle];

            if (selectedArticle.unread) {
                // jeśli nie przeczytane oznaczamy jako przeczytane
                [selectedArticle markAsUnread:NO];
                ArticleTableRowView *rowView = [self.headlinesTable rowViewAtRow:selectedRow makeIfNecessary:NO];
                rowView.unread = NO;
                [self.delegate feedsDataChanged:self];
            }

            if ([self.delegate respondsToSelector:@selector(feedStarSelectionChanged:)]) {
                [self.delegate feedStarSelectionChanged:selectedArticle.starred];
            }

            if ([self.delegate respondsToSelector:@selector(feedPublishSelectionChanged:)]) {
                [self.delegate feedPublishSelectionChanged:selectedArticle.published];
            }
        } else {
            ArticleTableMoreRowView *rowView = [self.headlinesTable rowViewAtRow:selectedRow makeIfNecessary:NO];

            [rowView.spinner startAnimation:self];

            [self.headlinesTable deselectRow:selectedRow];

            TinyTinyRSSFeed *feed = (TinyTinyRSSFeed *)self.categoryItem;
            NSUInteger start;
            if (feed.articlesVisibility == MGBArticleVisibilityUnread) {
                start = 1;
                for (TinyTinyRSSArticle *article in self.headlinesArray) {
                    if (article.unread) {
                        start++;
                    }
                }
            } else {
                start = [self.headlinesArray count] + 1;
            }

            [self.category downloadMoreArticles:^(NSArray *articles) {
                [rowView.spinner stopAnimation:self];

                if (articles.count == 0) {
                    self.isAllArticlesLoaded = YES;
                } else {
                    [self.headlinesArray addObjectsFromArray:articles];
                }

                [self.headlinesTable reloadData];
            } startFrom:start];
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSTableRowView *)tableView:(NSTableView *)tableView rowViewForRow:(NSInteger)row
{
    if (self.headlinesArray.count > row) {
        ArticleTableRowView *rowView = [tableView makeViewWithIdentifier:staticArticleViewIdentifier owner:self];

        rowView.tableRow = row;
        rowView.delegate = self;

        TinyTinyRSSArticle *article = [self.headlinesArray objectAtIndex:(NSUInteger)row];

        rowView.titleLabel.stringValue       = article.articleTitle;
        rowView.excerptLabel.stringValue     = article.excerpt;

        if (article.feed) {
            rowView.feedTitleLabel.stringValue = article.feed.name;
            rowView.iconView.image = article.feed.icon;
        }

        if (staticUpdateTimeDisplayMethod == MGB_UPDATE_TIME_DISPLAY_ELAPSED_FULL_DATE) {
            rowView.elapsedTimeLabel.stringValue = [article updateTimeStringFull];
        } else {
            rowView.elapsedTimeLabel.stringValue = [article updateTimeString];
        }

        NSImage *starImage       = article.starred ? [NSImage imageNamed:@"star_24"] : [NSImage imageNamed:@"star_off_24"];
        rowView.starButton.image = starImage;
        rowView.starButton.tag   = (NSInteger)article.starred;

        NSImage *publishImage       = article.published ? [NSImage imageNamed:@"rss_24"] : [NSImage imageNamed:@"rss_off_24"];
        rowView.publishButton.image = publishImage;
        rowView.publishButton.tag   = (NSInteger)article.published;

        rowView.unread = [article unread];

        return rowView;
    } else {
        ArticleTableMoreRowView *rowView = [tableView makeViewWithIdentifier:@"ArticleTableMoreRowViewKey" owner:self];

        rowView.unread = true;

        return rowView;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (CGFloat)tableView:(NSTableView *)tableView heightOfRow:(NSInteger)row
{
    return 56;
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark - WebPolicyDelegate
- (void)webView:(WebView *)webView decidePolicyForNavigationAction:(NSDictionary *)actionInformation request:(NSURLRequest *)request frame:(WebFrame *)frame
        decisionListener:(id < WebPolicyDecisionListener >)listener
{
    if ([actionInformation objectForKey:@"WebActionElementKey"]) {
        [[NSWorkspace sharedWorkspace] openURL:[request URL]];
    } else {
        [listener use];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)webView:(WebView *)webView decidePolicyForNewWindowAction:(NSDictionary *)actionInformation request:(NSURLRequest *)request
   newFrameName:(NSString *)frameName decisionListener:(id < WebPolicyDecisionListener >)listener
{
    [[NSWorkspace sharedWorkspace] openURL:[request URL]];
}



// ---------------------------------------------------------------------------------------------------------------------
#pragma mark - Zmienne statyczne
+ (void)setUpdateTimeDisplayMethod:(unsigned char)type
{
    staticUpdateTimeDisplayMethod = type;
}

// ---------------------------------------------------------------------------------------------------------------------
+ (unsigned char)updateTimeDisplayMethod
{
    return staticUpdateTimeDisplayMethod;
}

// ---------------------------------------------------------------------------------------------------------------------
+ (void)setArticleViewIdentifier:(NSString *)identifier
{
    staticArticleViewIdentifier = identifier;
}

// ---------------------------------------------------------------------------------------------------------------------
+ (NSString *)articleViewIdentifier
{
    return staticArticleViewIdentifier;
}

@end
