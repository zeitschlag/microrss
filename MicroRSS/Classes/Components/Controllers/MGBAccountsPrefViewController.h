//
//  MGBAccountsPrefViewController.h
//  MicroRSS
//
//  Created by Grzesiek on 31.08.2013.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <MASPreferences/MASPreferencesViewController.h>

/**
* Kontroler odpowiedzialny za zarządzanie kontami w preferencjach programu
*/
@interface MGBAccountsPrefViewController : NSViewController <MASPreferencesViewController, NSTableViewDataSource, NSTableViewDelegate>

// tablica z listą serwisów (kont) do wyświetlenia
@property NSMutableArray *servicesArray;

// Tabela z listą kont
@property (weak) IBOutlet NSTableView *accountsTable;

// Pola z formularza
@property (weak) IBOutlet NSTextField *nameField;

@property (weak) IBOutlet NSTextField *loginField;

@property (weak) IBOutlet NSSecureTextField *passwordField;

@property (weak) IBOutlet NSTextField *urlField;

// Akcja dodawania nowego serwisu
- (IBAction)showAddWindow:(id)sender;

// Akcja uruchamiana po zamknięciu okna dodawania nowego serwisu
- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo;

// Kasuje zaznaczone konto
- (IBAction)deleteSelectedAccount:(id)sender;

// Uruchamiane po potwierdzeniu przez użytkownika usunięcia konta
- (void)deleteAlertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode;
@end
