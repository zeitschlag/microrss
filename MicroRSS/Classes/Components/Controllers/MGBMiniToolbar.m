//
// Created by Grzesiek on 05.09.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import "MGBMiniToolbar.h"

@implementation MGBMiniToolbar

// ---------------------------------------------------------------------------------------------------------------------
- (void)drawRect:(NSRect)dirtyRect
{
    //// Color Declarations
    NSColor* border = [NSColor colorWithCalibratedRed: 0.58 green: 0.58 blue: 0.58 alpha: 1];
    NSColor *gradientColor = [NSColor colorWithCalibratedRed: 0.912 green: 0.912 blue: 0.912 alpha: 1];

    //// Gradient Declarations
    NSGradient *gradient = [[NSGradient alloc] initWithStartingColor:gradientColor endingColor:[NSColor whiteColor]];

    //// Rectangle Drawing
    NSBezierPath* rectanglePath = [NSBezierPath bezierPathWithRect: self.bounds];
    [gradient drawInBezierPath:rectanglePath angle:-90];
    [border setStroke];
    [rectanglePath setLineWidth: 1];
    [rectanglePath stroke];
}

// ---------------------------------------------------------------------------------------------------------------------

@end