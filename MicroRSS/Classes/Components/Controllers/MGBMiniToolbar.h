//
// Created by Grzesiek on 05.09.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>

/**
* Klasa służąca do wyświetlania gradientowego podkładu dla mini paska narzędzi
*/
@interface MGBMiniToolbar : NSView
@end