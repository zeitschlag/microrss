//
//  MGBArticleViewController.h
//  MicroRSS
//
//  Created by Grzesiek on 06/22/13.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import "ArticleTableRowViewDelegate.h"

#define MGB_UPDATE_TIME_DISPLAY_ELAPSED_DAYS 0
#define MGB_UPDATE_TIME_DISPLAY_ELAPSED_FULL_DATE 1

@class DataServiceTreeItem;
@class WebView;
@protocol MGBArticleViewControllerDelegate;
@class MGBArticlePreviewWindowController;

@interface MGBArticleViewController : NSViewController <NSTableViewDataSource, NSTableViewDelegate, ArticleTableRowViewDelegate>

/**
* Ustawia dane aktualnie wyświetlanej kategorii
*
* @param item kategoria
*/
- (void)setCategoryItem:(DataServiceTreeItem *)item;

/**
* Przeładowuje listę artykułów jeśli te uległy zmianie
*/
- (void)updateArticlesIfNeeded;

/**
* Akcja uruchamiana po dwukrotnym kliknięciu elementu w tabeli
*/
- (IBAction)goToWebPage:(id)sender;

/**
* Akcja oznaczająca dany artykuł jako nieprzeczytany
*/
- (IBAction)markAsUnread:(id)sender;

/**
* Zwraca aktualnie ustawioną kategorię
*/
- (DataServiceTreeItem *)categoryItem;

/**
* Przeładowuje listę artykułów
*/
- (void)reloadArticlesList;

/**
* Tabela z nagłówkami
*/
@property (weak) IBOutlet NSTableView *headlinesTable;

/**
 * Spinner informujący o wczytywaniu danych
 */
@property (weak) IBOutlet NSProgressIndicator *loadingIndicator;

/**
 * Komponent wyświetlający ikonkę feed'a
 */
@property (weak) IBOutlet NSImageView *feedIconImageView;

/**
 * Etykieta zawierająca tytuł feed'a
 */
@property (weak) IBOutlet NSTextField *feedTitleLabel;

/**
* Widok z podglądem treści artykułu
*/
@property (weak) IBOutlet WebView *webView;

/**
* Przełącznik zmieniający kolejność sortowania artykułów
*/
@property (weak) IBOutlet NSSegmentedControl *sortOrderControll;

/**
* Przełącznik zmieniający filtr wyświetlanych artykułów z nieprzeczytanych na wszystkie
*/
@property (weak) IBOutlet NSSegmentedControl *unreadFilterControll;

/**
* Delegat
*/
@property (nonatomic, assign) id<MGBArticleViewControllerDelegate> delegate;

/**
* Numer ID artykułu który ma zostać zaznaczony zaraz po załadowaniu danych.
*/
@property (nonatomic, assign) NSInteger articleToSelect;

/**
* Ustawia aktualnie używany sposób wyświetlania daty
*/
+ (void)setUpdateTimeDisplayMethod:(unsigned char)type;

/**
* Zwraca aktualnie używany sposób wyświetlania daty
*/
+ (unsigned char)updateTimeDisplayMethod;

/**
* Ustawia identyfikator dla widoku wyświetlającego artykuł na liście
*/
+ (void)setArticleViewIdentifier:(NSString *)identifier;

/**
* Zwraca identyfikator dla widoku wyświetlającego artykuł na liście
*/
+ (NSString *)articleViewIdentifier;

/**
* Drukowanie aktualnie zaznaczonego artykułu
*/
- (IBAction)print:(id)sender;

/**
* Akcja podpięta do guzika sortowania i zmieniająca sposób sortowania artykułów
*/
- (IBAction)changeSortOrder:(id)sender;

/**
* Akcja podpięta do guzika filtra zmieniająca sposób filtrowania
*/
- (IBAction)changeUnreadFilter:(id)sender;
@end
