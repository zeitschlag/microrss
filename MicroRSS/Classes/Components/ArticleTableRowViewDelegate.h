//
// Created by Grzesiek on 18.08.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>

@protocol ArticleTableRowViewDelegate <NSObject>

@required

/**
* Oznacza dany wiersz gwiazdką
*/
- (void)markArticleForRow:(NSInteger)row starred:(BOOL)starred;

/**
* Oznacza dany wiersz jako opublikowany
*/
- (void)markArticleForRow:(NSInteger)row published: (BOOL)published;

/**
* Otwiera podgląd danego artykułu
*/
- (void)showArticlePreviewForRow:(NSInteger)row;

@end