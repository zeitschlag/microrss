//
// Created by Grzesiek on 25.06.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import "ArticleTableRowView.h"
#import "ArticleTableRowViewDelegate.h"

@interface ArticleTableRowView ()

// Zwraca gradient tła
+ (NSGradient *)backgroundGradient;

// Zwraca gradient dla zaznaczenia
+ (NSGradient *)selectionGradient;

// Zwraca kolor separatora
+ (NSColor *)separatorColor;
@end

@implementation ArticleTableRowView

// ---------------------------------------------------------------------------------------------------------------------
- (void)drawBackgroundInRect:(NSRect)dirtyRect
{
    if (self.unread) {
        [[[self class] backgroundGradient] drawInRect:self.bounds angle:90];
    } else {
        [[[self class] unreadGradient] drawInRect:self.bounds angle:90];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSRect)separatorRect
{
    NSRect separatorRect = self.bounds;
    separatorRect.origin.y = NSMaxY(separatorRect) - 1;
    separatorRect.size.height = 1;
    return separatorRect;
}

// ---------------------------------------------------------------------------------------------------------------------
// Only called if the table is set with a horizontal grid
- (void)drawSeparatorInRect:(NSRect)dirtyRect
{
    [[[self class] separatorColor] set];

    NSRectFill([self separatorRect]);
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)drawSelectionInRect:(NSRect)dirtyRect
{
    if (self.selectionHighlightStyle != NSTableViewSelectionHighlightStyleNone) {
        [[[self class] selectionGradient] drawInRect:self.bounds angle:270];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
// Metoda nadpisana aby przy ustawieniu zaznaczenia zmienić kolor tekstu
- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];

    if (selected) {
        self.titleLabel.textColor       = [NSColor whiteColor];
        self.excerptLabel.textColor     = [NSColor whiteColor];
        self.elapsedTimeLabel.textColor = [NSColor whiteColor];
        self.feedTitleLabel.textColor   = [NSColor whiteColor];
    } else if (!self.unread) {
        self.titleLabel.textColor       = [NSColor grayColor];
        self.excerptLabel.textColor     = [NSColor grayColor];
        self.elapsedTimeLabel.textColor = [NSColor grayColor];
        self.feedTitleLabel.textColor   = [NSColor grayColor];
    } else {
        self.titleLabel.textColor       = [NSColor blackColor];
        self.excerptLabel.textColor     = [NSColor darkGrayColor];
        self.elapsedTimeLabel.textColor = [NSColor darkGrayColor];
        self.feedTitleLabel.textColor   = [NSColor blackColor];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)setUnread:(BOOL)unread
{
    _unread = unread;

    if (self.selected) {
        self.titleLabel.textColor       = [NSColor whiteColor];
        self.excerptLabel.textColor     = [NSColor whiteColor];
        self.elapsedTimeLabel.textColor = [NSColor whiteColor];
        self.feedTitleLabel.textColor   = [NSColor whiteColor];
    } else if (!unread) {
        self.titleLabel.textColor       = [NSColor grayColor];
        self.excerptLabel.textColor     = [NSColor grayColor];
        self.elapsedTimeLabel.textColor = [NSColor grayColor];
        self.feedTitleLabel.textColor   = [NSColor grayColor];
    } else {
        self.titleLabel.textColor       = [NSColor blackColor];
        self.excerptLabel.textColor     = [NSColor darkGrayColor];
        self.elapsedTimeLabel.textColor = [NSColor darkGrayColor];
        self.feedTitleLabel.textColor   = [NSColor blackColor];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)starButtonClicked:(id)sender
{
    NSButton *button = (NSButton *)sender;

    if (button.tag == 1) {
        button.tag   = 0;
        button.image = [NSImage imageNamed:@"star_off_24"];
    } else {
        button.tag   = 1;
        button.image = [NSImage imageNamed:@"star_24"];
    }

    [self.delegate markArticleForRow:self.tableRow starred:button.tag == 1];
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)publishButtonClicked:(id)sender
{
    NSButton *button = (NSButton *)sender;

    if (button.tag == 1) {
        button.tag   = 0;
        button.image = [NSImage imageNamed:@"rss_off_24"];
    } else {
        button.tag   = 1;
        button.image = [NSImage imageNamed:@"rss_24"];
    }

    [self.delegate markArticleForRow:self.tableRow published:button.tag == 1];
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)previewButtinClicked:(id)sender
{
    [self.delegate showArticlePreviewForRow:self.tableRow];
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSBackgroundStyle)interiorBackgroundStyle
{
    return NSBackgroundStyleLight;
}

// ---------------------------------------------------------------------------------------------------------------------
+ (NSGradient *)backgroundGradient
{
    static NSGradient *gradient = nil;

    if (!gradient) {
        NSColor* gradientStart = [NSColor colorWithCalibratedRed: 0.898 green: 0.898 blue: 0.898 alpha: 1];
        NSColor* gradientStop = [NSColor colorWithCalibratedRed: 0.992 green: 0.992 blue: 0.992 alpha: 1];

        gradient = [[NSGradient alloc] initWithStartingColor: gradientStop endingColor: gradientStart];
    }

    return gradient;
}

// ---------------------------------------------------------------------------------------------------------------------
+ (NSGradient *)selectionGradient
{
    static NSGradient *gradient = nil;

    if (!gradient) {
        NSColor* gradientStart = [NSColor colorWithCalibratedRed: 0.412 green: 0.588 blue: 0.824 alpha: 1];
        NSColor* gradientStop = [NSColor colorWithCalibratedRed: 0.225 green: 0.449 blue: 0.794 alpha: 1];

        gradient = [[NSGradient alloc] initWithStartingColor: gradientStop endingColor: gradientStart];
    }

    return gradient;
}

// ---------------------------------------------------------------------------------------------------------------------
+ (NSGradient *)unreadGradient
{
    static NSGradient *gradient = nil;

    if (!gradient) {
        NSColor* gradientStart = [NSColor colorWithCalibratedRed: 0.867 green: 0.875 blue: 0.89 alpha: 1];
        NSColor* gradientStop = [NSColor colorWithCalibratedRed: 0.812 green: 0.82 blue: 0.835 alpha: 1];

        gradient = [[NSGradient alloc] initWithStartingColor: gradientStop endingColor: gradientStart];
    }

    return gradient;
}

// ---------------------------------------------------------------------------------------------------------------------
+ (NSColor *)separatorColor
{
    static NSColor *color = nil;

    if (!color) {
        color = [NSColor colorWithSRGBRed:0.79 green:0.79 blue:0.79 alpha:1.0];
    }

    return color;
}

// ---------------------------------------------------------------------------------------------------------------------

@end
