//
// Created by Grzesiek on 27.07.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import "FeedTitleView.h"


@interface FeedTitleView ()
+ (NSGradient *)backgroundGradient;
@end

@implementation FeedTitleView

// ---------------------------------------------------------------------------------------------------------------------
- (void)drawRect:(NSRect)dirtyRect
{
    [[[self class] backgroundGradient] drawInRect:self.bounds angle:90];

    [[NSColor grayColor] set];
    NSRectFill(CGRectMake(0, 0, NSWidth(self.bounds), 1));
}

// ---------------------------------------------------------------------------------------------------------------------
+ (NSGradient *)backgroundGradient
{
    static NSGradient *gradient = nil;

    if (!gradient) {
        NSColor* gradientStart = [NSColor colorWithCalibratedRed: 0.968 green: 0.968 blue: 0.968 alpha: 1];
        NSColor* gradientStop = [NSColor colorWithCalibratedRed: 0.870 green: 0.870 blue: 0.870 alpha: 1];

        gradient = [[NSGradient alloc] initWithStartingColor: gradientStop endingColor: gradientStart];
    }

    return gradient;
}


@end