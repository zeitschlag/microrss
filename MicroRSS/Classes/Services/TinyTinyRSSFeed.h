//
// Created by Grzesiek on 01.06.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "DataServiceTreeItem.h"

// Stałe definiujące kolejność sortowania
typedef NS_ENUM(NSInteger, MGBSortingOrder) {
    MGBSortingOrderDescending,
    MGBSortingOrderAscending
};

// Stałe definiujące widoczność feedów
typedef NS_ENUM(NSInteger, MGBArticleVisibility) {
    MGBArticleVisibilityAll,
    MGBArticleVisibilityUnread
};

@class TinyTinyRSS;


@interface TinyTinyRSSFeed : DataServiceTreeItem

@property (nonatomic, assign) NSInteger dbID;

@property (nonatomic, assign) BOOL onTheServer;

@property (nonatomic, assign) NSInteger feedID;

@property (nonatomic, assign) NSInteger categoryID;

@property (nonatomic, retain) NSString *name;

@property (nonatomic, assign) NSInteger unread;

@property (nonatomic, assign) NSInteger lastUpdate;

@property (nonatomic, assign) BOOL hasFeedIcon;

@property (nonatomic, assign) TinyTinyRSS* service;

@property (nonatomic, retain) NSMutableArray *feeds;

@property (nonatomic, assign) BOOL isCategory;

@property (nonatomic, assign) NSInteger lastServerUpdate;

@property (nonatomic, assign) NSInteger orderID;

/**
* Kategoria w której znajduje się dany feed.
*/
@property (nonatomic, assign) TinyTinyRSSFeed *categoryFeed;

/**
* Porządek sortowania artykułów
*/
@property (nonatomic, assign) MGBSortingOrder articlesSortOrder;

/**
* Widoczność artykułów
*/
@property (nonatomic, assign) MGBArticleVisibility articlesVisibility;

/**
* Inicjalizuje obiekt na podstawie słownika odczytanego przez przetważanie odpowiedzi JSON z serwera
*
* @param dictionary słownik użyty do inicjalizacji
*/
- (id)initWithDictionary:(NSDictionary *)dictionary;

/**
* Inicjalizuje daną kategorię na podstawie słownika odczytanego przez przetważanie odpowiedzi JSON z serwera
*
* @param dictionary słownik użyty do inicjalizacji
*/
- (id)initWithCategoryDictionary:(NSDictionary *)dictionary;

/**
* Inicjalizuje daną kategorię na podstawie słownika odczytanego z bazy danych
*
* @param dictionary słownik użyty do inicjalizacji
*/
- (id)initWithDatabaseCategoryDictionary:(NSDictionary *)dictionary;

/**
* Inicjalizuje dany feed na podstawie słownika odczytanego z bazy danych
*
* @param dictionary słownik użyty do inicjalizacji
*/
- (id)initWithDatabaseDictionary:(NSDictionary *)dictionary;

/**
* Kasuje wszystkie widoczne feedy
*/
- (void)clearVisibleFeeds;

/**
* Wczytuje ikonkę z pliku o podanym adresie URL
*
* @param url adres do pliku z ikonką
*/
- (void)loadIconFromURL:(NSURL *)url;

/**
* Sprawdza czy dane artykułów mogły ulec zmianie
*/
- (BOOL)isArticleDataChanged;

/**
* Pobiera nagłówki z serwera w sposób asynchroniczny i wywołuje metodę callback gdy zakończy pracę.
*/
- (void)downloadArticles:(void (^)(NSMutableArray *))callback;

/**
* Pobiera więcej artykułów z serwera.
*/
- (void)downloadMoreArticles:(void (^)(NSArray *))callback startFrom:(NSUInteger)start;

/**
* Pobiera nowe artykuły.
*/
- (void)downloadNewArticles:(void (^)(NSArray *))callback;

/**
* Oznacza wszystkie artykuły jako przeczytane
*/
- (void)markAllAsRead:(void (^)(void))callback;

/**
* Przeprowadza operację wypisania się z kanału
*/
- (void)unsubscribe:(void (^)(void))callback;

/**
* Anuluje pobierane artykułów jeśli jakieś jest w trakcie.
*/
- (void)cancelArticleDownload;

/**
* Aktualizuje dane kopiując je z innego feed'a
*/
- (void)updateDataFromFeed:(TinyTinyRSSFeed *)feed;

/**
* Zmniejsza licznik nieprzeczytanych wiadomości o 1
*/
- (void)decreaseUnreadCount;

/**
* Zwiększa licznik nieprzeczytanych wiadomości o 1
*/
- (void)increaseUnreadCount;

/**
* Synchronizuje feed z bazą danych
*/
- (void)syncWithDatabase;

/**
* Kasuje wszystkie dane wskazanego feed'a (dane z bazy ikonkę oraz dane feedów podległych)
*/
- (void)removeLocalData;

/**
* Kasuje wszystkie feedy nie oznaczone jako nie obecne na serwerze
*/
- (void)removeFeedsNotOnServer;

/**
* Dodaje feed do listy wcześniej upewniając się czy go już tam nie ma
*
* @param feed dodawany feed
*/
- (BOOL)addFeed:(TinyTinyRSSFeed *)feed;
@end