//
// Created by Grzesiek on 23.06.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>

@class TinyTinyRSSFeed;
@class FMResultSet;

@interface TinyTinyRSSArticle : NSObject

@property NSInteger dbID;

@property (nonatomic, assign) NSInteger feedID;

@property (nonatomic, assign) NSInteger articleID;

@property (nonatomic, retain) NSString *articleTitle;

@property (nonatomic, assign) BOOL unread;

@property (nonatomic, assign) BOOL starred;

@property (nonatomic, assign) BOOL published;

@property (nonatomic, retain) NSString *link;

@property (nonatomic, retain) NSString* excerpt;

@property (nonatomic, retain) NSString *content;

@property (nonatomic, assign) NSInteger updated;

/**
* Feed do którego należy dany nagłówek
*/
@property (nonatomic, assign) TinyTinyRSSFeed *feed;

/**
* Inicjalizuje obiekt wartościami ze słownika danych przesłanych przez serwer
*/
- (id)initWithDictionary:(NSDictionary *)dictionary;

/**
* Inicjalizuje obiekt wartościami z bazy danych
*/
- (id)initWithResultSet:(FMResultSet *)resultSet;

/**
* Oznacza artykuł jako nieprzeczytany
*/
- (void)markAsUnread:(BOOL)unread;

/**
* Oznacza artykuł gwiazdką
*/
- (void)markAsStarred:(bool)starred;

/**
* Oznacza artykuł jako opublikowany
*/
- (void)markAsPublished:(bool)published;

/**
* Zwraca tekst z liczbą dni jakie upłynęły od konkretnej daty
*/
- (NSString *)updateTimeString;

/**
* Zwraca tekst z pełną datą publikacji
*/
- (NSString *)updateTimeStringFull;
@end