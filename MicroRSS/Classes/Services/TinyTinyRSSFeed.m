//
// Created by Grzesiek on 01.06.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import "TinyTinyRSSFeed.h"
#import "TinyTinyRSS.h"
#import "ASIHTTPRequest.h"
#import "TinyTinyRSSArticle.h"
#import "MicroRSSFeedTable.h"
#import "MicroRSSArticleTable.h"


@interface TinyTinyRSSFeed ()

// Ikonka dla danego feed'a
@property (nonatomic, retain) NSImage *feedIcon;

// Request aktualnie używany do pobierania artykułów
@property (nonatomic, retain) ASIHTTPRequest *articleRequest;

// Tablica z feed'ami widocznymi na liście
@property (nonatomic, retain) NSMutableArray *visibleFeeds;

// Pobiera nagłówki wykonując logowanie jeśli zajdzie taka potrzeba
- (void)downloadArticles:(void (^)(NSMutableArray *))callback performLoginIfNecessary:(BOOL)perform;

// Pobiera więcej artykułów przeprowadzając operację logowania jeśli zajdzie taka potrzeba.
- (void)downloadMoreArticles:(void (^)(NSMutableArray *))callback startFrom:(NSUInteger)start performLoginIfNecessary:(BOOL)perform;

// Pobiera nowe artykuły przeprowadzając operację logowania jeśli zajdzie taka potrzeba.
- (void)downloadNewArticles:(void (^)(NSArray *))callback performLoginIfNecessary:(BOOL)perform;

// zlicza niepobrane artykuły od daty ostatniego pobrania
- (void)countOffset:(void (^)(NSUInteger))callback performLoginIfNecessary:(BOOL)perform;

// oznacza wszystkie artykuły jako przeczytane
- (void)markAllAsRead:(void (^)(void))callback performLoginIfNecessary:(BOOL)perform;

// wypisuje się z kanału przeprowadzając operację logowania jeśli zajdzie taka potrzeba
- (void)unsubscribe:(void (^)(void))callback performLoginIfNecessary:(BOOL)perform;
@end

@implementation TinyTinyRSSFeed

// ---------------------------------------------------------------------------------------------------------------------
- (void)setArticlesSortOrder:(MGBSortingOrder)articlesSortOrder
{
    _articlesSortOrder = articlesSortOrder;

    if (self.dbID) {
        // kasowanie daty ostatniej aktualizacji tylko wtedy gdy feed jest w pełni zainicjalizowany
        self.lastServerUpdate = -1;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)setArticlesVisibility:(MGBArticleVisibility)articlesVisibility
{
    _articlesVisibility = articlesVisibility;

    if (self.dbID) {
        // kasowanie daty ostatniej aktualizacji tylko wtedy gdy feed jest w pełni zainicjalizowany
        self.lastServerUpdate = -1;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        self.name             = [dictionary objectForKey:@"title"];
        self.feedID           = [[dictionary objectForKey:@"id"] integerValue];
        self.unread           = [[dictionary objectForKey:@"unread"] integerValue];
        self.lastUpdate       = [[dictionary objectForKey:@"last_updated"] integerValue];
        self.categoryID       = [[dictionary objectForKey:@"cat_id"] integerValue];
        self.isCategory       = NO;
        self.onTheServer      = YES;
        self.lastServerUpdate = -1;
        self.orderID          = [[dictionary objectForKey:@"order_id"] integerValue];

        switch (self.feedID) {
            case 0:
                // archiwum
                self.feedIcon = [NSImage imageNamed:@"archive"];
                break;
            case -1:
                // ulubione
                self.feedIcon = [NSImage imageNamed:@"star"];
                break;
            case -2:
                // opublikowane
                self.feedIcon = [NSImage imageNamed:@"rss"];
                break;
            case -3:
                // świeże
                self.feedIcon = [NSImage imageNamed:@"fresh"];
                break;
            case -4:
                // wszystkie
                self.feedIcon = [NSImage imageNamed:@"all"];
                break;
            case -6:
                // ostatnio czytane
                self.feedIcon = [NSImage imageNamed:@"clock"];
                break;
            default:
                self.feedIcon = nil;
                break;
        }

        NSNumber *hasIcon = [dictionary objectForKey:@"has_icon"];
        self.hasFeedIcon  = [hasIcon boolValue];
    }

    return self;
}

// ---------------------------------------------------------------------------------------------------------------------
- (id)initWithCategoryDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        self.name             = [dictionary objectForKey:@"title"];
        self.feedID           = [(NSNumber *)[dictionary objectForKey:@"id"] integerValue];
        self.categoryID       = 0;
        self.unread           = [(NSNumber *)[dictionary objectForKey:@"unread"] integerValue];
        self.feeds            = [[NSMutableArray alloc] init];
        self.visibleFeeds     = [[NSMutableArray alloc] init];
        self.isCategory       = YES;
        self.onTheServer      = YES;
        self.feedIcon         = [[NSWorkspace sharedWorkspace] iconForFileType:NSFileTypeForHFSTypeCode(kGenericFolderIcon)];
        self.lastServerUpdate = -1;
        self.orderID          = [[dictionary objectForKey:@"order_id"] integerValue];
    }

    return self;
}

// ---------------------------------------------------------------------------------------------------------------------
- (id)initWithDatabaseCategoryDictionary:(NSDictionary *)dictionary
{
    self = [self initWithCategoryDictionary:dictionary];

    if (self) {
        self.articlesSortOrder  = [[dictionary objectForKey:@"article_sort_order"] integerValue] == 1 ? MGBSortingOrderAscending : MGBSortingOrderDescending;
        self.articlesVisibility = [[dictionary objectForKey:@"article_visibility"] integerValue] == 1 ? MGBArticleVisibilityUnread : MGBArticleVisibilityAll;
        self.dbID               = [[dictionary objectForKey:@"db_id"] integerValue];
        self.lastServerUpdate   = -1;
    }

    return self;
}

// ---------------------------------------------------------------------------------------------------------------------
- (id)initWithDatabaseDictionary:(NSDictionary *)dictionary
{
    self = [self initWithDictionary:dictionary];

    if (self) {
        self.articlesSortOrder  = [[dictionary objectForKey:@"article_sort_order"] integerValue] == 1 ? MGBSortingOrderAscending : MGBSortingOrderDescending;
        self.articlesVisibility = [[dictionary objectForKey:@"article_visibility"] integerValue] == 1 ? MGBArticleVisibilityUnread : MGBArticleVisibilityAll;
        self.dbID               = [[dictionary objectForKey:@"db_id"] integerValue];
        self.lastServerUpdate   = [[dictionary objectForKey:@"last_server_update"] integerValue];
    }

    return self;
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)addFeed:(TinyTinyRSSFeed *)feed
{
    NSUInteger feedIndex = [self.feeds indexOfObject:feed];

    if (feedIndex != NSNotFound) {
        // aktualizacja feed'a
        TinyTinyRSSFeed *existingFeed = [self.feeds objectAtIndex:feedIndex];
        [existingFeed updateDataFromFeed:feed];
        if (feed.dbID) {
            existingFeed.dbID = feed.dbID;
        }

        if ([TinyTinyRSS feedsVisibility] == MGB_FEEDS_VISIBILITY_UNREAD) {
            if (existingFeed.unread > 0) {
                [self.visibleFeeds addObject:existingFeed];
            }
        } else {
            [self.visibleFeeds addObject:existingFeed];
        }

        return NO;
    } else {
        [self.feeds addObject:feed];

        feed.categoryFeed = self;

        if ([TinyTinyRSS feedsVisibility] == MGB_FEEDS_VISIBILITY_UNREAD) {
            if (feed.unread > 0) {
                [self.visibleFeeds addObject:feed];
            }
        } else {
            [self.visibleFeeds addObject:feed];
        }

        return YES;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)clearVisibleFeeds
{
    [self.visibleFeeds removeAllObjects];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)loadIconFromURL:(NSURL *)url
{
    self.feedIcon = [[NSImage alloc] initWithContentsOfURL:url];
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)isArticleDataChanged
{
    return self.lastUpdate != self.lastServerUpdate;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)downloadArticles:(void (^)(NSMutableArray *))callback
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        if (self.lastUpdate != self.lastServerUpdate) {
            [self downloadArticles:callback performLoginIfNecessary:YES];
        } else {
            NSMutableArray *articles = [MicroRSSArticleTable fetchTTRSSArticlesForFeed:self andService:self.service];

            dispatch_async(dispatch_get_main_queue(), ^{
                // powrót na wątek główny
                callback(articles);
            });
        }
    });
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)downloadArticles:(void (^)(NSMutableArray *))callback performLoginIfNecessary:(BOOL)perform
{
    if (self.articleRequest.isExecuting) {
        [self.articleRequest cancel];
        self.articleRequest = nil;
    }

    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.service.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSDictionary *opDict;
    opDict = @{@"op" : @"getHeadlines", @"feed_id": [NSNumber numberWithInteger:self.feedID], @"is_cat": [NSNumber numberWithBool:self.isCategory],
            @"show_excerpt": @YES, @"show_content" : @YES, @"skip": @0, @"limit": @50, @"sid" : self.service.sessionIdentifier,
            @"order_by": self.articlesSortOrder == MGBSortingOrderAscending ? @"date_reverse" : @"default",
            @"view_mode": self.articlesVisibility == MGBArticleVisibilityAll ? @"all_articles" : @"unread"};

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    self.articleRequest = request;

    [request setCompletionBlock:^{
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[self.articleRequest responseData] options:0 error:nil];
              
        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            NSArray *content = [responseDict objectForKey:@"content"];

            NSMutableArray *resultArray = [[NSMutableArray alloc] initWithCapacity:content.count];
            for (NSDictionary *dictionary in content) {
                TinyTinyRSSArticle *headline = [[TinyTinyRSSArticle alloc] initWithDictionary:dictionary];

                if (headline.feedID == self.feedID) {
                    headline.feed = self;
                } else {
                    // wyszukiwanie feeda do którego należy dany artykuł
                    NSNumber *feedIDKey = [[NSNumber alloc] initWithInteger:headline.feedID];
                    TinyTinyRSSFeed *parentFeed = [self.service.feedsIndex objectForKey:feedIDKey];

                    if (parentFeed) {
                        headline.feed = parentFeed;
                    } else {
                        headline.feed = self;
                    }
                }

                [resultArray addObject:headline];
            }

            if (self.feedID != -6) {
                // ostatnio przeczytane artykuły nie są cacheowane w bazie
                self.lastServerUpdate = self.lastUpdate;
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                callback(resultArray);
            });

            [MicroRSSArticleTable updateTTRSSArticles:resultArray forFeed:self append:NO];
        } else {
            NSDictionary *content = [responseDict objectForKey:@"content"];

            if ([(NSString *)[content objectForKey:@"error"] isEqualToString:@"NOT_LOGGED_IN"] && perform) {
                // nie zalogowano - trzeba się ponownie zalogować z jeszcze raz wywołać pobieranie nagłówka
                dispatch_async(dispatch_get_global_queue(0, 0), ^{
                    // wątek w tle
                    if ([self.service performLogin]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // wątek główny
                            [self downloadArticles:callback performLoginIfNecessary:NO];
                        });
                    } else {
                        [self.service errorNotLoggedIn];
                    }
                });
            } else {
                [self.service errorTTRSS:[content objectForKey:@"error"]];
            }

            NSMutableArray *articles = [MicroRSSArticleTable fetchTTRSSArticlesForFeed:self andService:self.service];

            dispatch_async(dispatch_get_main_queue(), ^{
                // powrót na wątek główny
                callback(articles);
            });
        }

        self.articleRequest = nil;
    }];

    [request setFailedBlock:^{
        [self.service errorConnection:self.articleRequest.error];

        self.articleRequest = nil;

        NSMutableArray *articles = [MicroRSSArticleTable fetchTTRSSArticlesForFeed:self andService:self.service];

        dispatch_async(dispatch_get_main_queue(), ^{
            // powrót na wątek główny
            callback(articles);
        });
    }];

    [request startAsynchronous];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)downloadMoreArticles:(void (^)(NSArray *))callback startFrom:(NSUInteger)start
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        if (self.articlesSortOrder == MGBSortingOrderDescending) {
            [self countOffset:^(NSUInteger offset) {
                [self downloadMoreArticles:^(NSMutableArray *array) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        callback(array);
                    });
                } startFrom:start + offset performLoginIfNecessary:YES];
            } performLoginIfNecessary:YES];
        } else {
            [self downloadMoreArticles:^(NSMutableArray *array) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    callback(array);
                });
            } startFrom:start performLoginIfNecessary:YES];
        }
    });
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)downloadMoreArticles:(void (^)(NSMutableArray *))callback startFrom:(NSUInteger)start performLoginIfNecessary:(BOOL)perform
{
    if (self.articleRequest.isExecuting) {
        [self.articleRequest cancel];
        self.articleRequest = nil;
    }

    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.service.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSDictionary *opDict;
    opDict = @{@"op" : @"getHeadlines", @"feed_id": [NSNumber numberWithInteger:self.feedID], @"is_cat": [NSNumber numberWithBool:self.isCategory],
            @"show_excerpt": @YES, @"show_content" : @YES, @"skip": [NSNumber numberWithUnsignedInteger:start],
            @"limit": @50, @"sid" : self.service.sessionIdentifier, @"order_by": self.articlesSortOrder == MGBSortingOrderAscending ? @"date_reverse" : @"default",
            @"view_mode": self.articlesVisibility == MGBArticleVisibilityAll ? @"all_articles" : @"unread"};

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    self.articleRequest = request;

    [request setCompletionBlock:^{
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[self.articleRequest responseData] options:0 error:nil];

        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            NSArray *content = [responseDict objectForKey:@"content"];

            NSMutableArray *resultArray = [[NSMutableArray alloc] initWithCapacity:content.count];
            for (NSDictionary *dictionary in content) {
                TinyTinyRSSArticle *headline = [[TinyTinyRSSArticle alloc] initWithDictionary:dictionary];

                if (headline.feedID == self.feedID) {
                    headline.feed = self;
                } else {
                    // wyszukiwanie feeda do którego należy dany artykuł
                    NSNumber *feedIDKey = [[NSNumber alloc] initWithInteger:headline.feedID];
                    TinyTinyRSSFeed *parentFeed = [self.service.feedsIndex objectForKey:feedIDKey];

                    if (parentFeed) {
                        headline.feed = parentFeed;
                    } else {
                        headline.feed = self;
                    }
                }

                [resultArray addObject:headline];
            }

            callback(resultArray);

            [MicroRSSArticleTable updateTTRSSArticles:resultArray forFeed:self append:YES];
        } else {
            NSDictionary *content = [responseDict objectForKey:@"content"];

            if ([(NSString *)[content objectForKey:@"error"] isEqualToString:@"NOT_LOGGED_IN"] && perform) {
                // nie zalogowano - trzeba się ponownie zalogować z jeszcze raz wywołać pobieranie nagłówka
                dispatch_async(dispatch_get_global_queue(0, 0), ^{
                    // wątek w tle
                    if ([self.service performLogin]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // wątek główny
                            [self downloadMoreArticles:callback startFrom:start performLoginIfNecessary:NO];
                        });
                    } else {
                        [self.service errorNotLoggedIn];
                    }
                });
            } else {
                [self.service errorTTRSS:[content objectForKey:@"error"]];

                callback([NSMutableArray array]);
            }
        }

        self.articleRequest = nil;
    }];

    [request setFailedBlock:^{
        [self.service errorConnection:self.articleRequest.error];

        self.articleRequest = nil;
        callback([NSMutableArray array]);
    }];

    [request startAsynchronous];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)downloadNewArticles:(void (^)(NSArray *))callback
{
    [self downloadNewArticles:callback performLoginIfNecessary:YES];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)downloadNewArticles:(void (^)(NSArray *))callback performLoginIfNecessary:(BOOL)perform
{
    if (self.articleRequest.isExecuting) {
        return;
    }

    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.service.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSInteger maxArticleID = [MicroRSSArticleTable articleMaxID:self.dbID];

    NSDictionary *opDict = @{@"op" : @"getHeadlines", @"feed_id": [NSNumber numberWithInteger:self.feedID], @"is_cat": @NO,
            @"show_excerpt": @YES, @"since_id": [NSNumber numberWithInt:maxArticleID], @"show_content" : @YES,
            @"view_mode": @"all_articles", @"skip": @0, @"limit": @50, @"sid" : self.service.sessionIdentifier};
    NSData *jsonData     = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    self.articleRequest = request;

    [request setCompletionBlock:^{
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[self.articleRequest responseData] options:0 error:nil];

        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            NSArray *content = [responseDict objectForKey:@"content"];

            NSMutableArray *resultArray = [[NSMutableArray alloc] initWithCapacity:content.count];
            for (NSDictionary *dictionary in content) {
                TinyTinyRSSArticle *headline = [[TinyTinyRSSArticle alloc] initWithDictionary:dictionary];

                if (headline.feedID == self.feedID) {
                    headline.feed = self;
                } else {
                    // wyszukiwanie feeda do którego należy dany artykuł
                    NSNumber *feedIDKey = [[NSNumber alloc] initWithInteger:headline.feedID];
                    TinyTinyRSSFeed *parentFeed = [self.service.feedsIndex objectForKey:feedIDKey];

                    if (parentFeed) {
                        headline.feed = parentFeed;
                    } else {
                        headline.feed = self;
                    }
                }

                [resultArray addObject:headline];
            }

            if (resultArray.count > 0) {
                // jeśli pobrano jakieś artykuły to zapisujemy ID najnowszego artykułu
                TinyTinyRSSArticle *newestArticle = [resultArray objectAtIndex:0];

                [MicroRSSArticleTable saveArticleMaxID:newestArticle.articleID forFeed:self.dbID];
            }

            if (self.articlesSortOrder == MGBSortingOrderDescending && self.articlesVisibility == MGBArticleVisibilityAll) {
                [MicroRSSArticleTable addNewTTRSSArticles:resultArray forFeed:self];
                if (self.feedID != -6) {
                    // ostatnio przeczytane artykuły nie są cacheowane w bazie
                    self.lastServerUpdate = self.lastUpdate;
                }
            }

            callback(resultArray);
        } else {
            NSDictionary *content = [responseDict objectForKey:@"content"];

            if ([(NSString *)[content objectForKey:@"error"] isEqualToString:@"NOT_LOGGED_IN"] && perform) {
                // nie zalogowano - trzeba się ponownie zalogować z jeszcze raz wywołać pobieranie nagłówka
                dispatch_async(dispatch_get_global_queue(0, 0), ^{
                    // wątek w tle
                    if ([self.service performLogin]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // wątek główny
                            [self downloadNewArticles:callback performLoginIfNecessary:NO];
                        });
                    }
                });
            }
        }

        self.articleRequest = nil;
    }];

    [request setFailedBlock:^{
        [self.service errorConnection:self.articleRequest.error];

        self.articleRequest = nil;
    }];

    [request startAsynchronous];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)countOffset:(void (^)(NSUInteger))callback performLoginIfNecessary:(BOOL)perform
{
    if (self.articleRequest.isExecuting) {
        [self.articleRequest cancel];
        self.articleRequest = nil;
    }

    int maxArticleID = [MicroRSSArticleTable topArticleIDForFeedID:self.dbID];

    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.service.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSDictionary *opDict;
    opDict = @{@"op" : @"getHeadlines", @"feed_id": [NSNumber numberWithInteger:self.feedID], @"is_cat": [NSNumber numberWithBool:self.isCategory],
            @"show_excerpt": @NO, @"show_content" : @NO, @"since_id": [NSNumber numberWithInt:maxArticleID],
            @"skip": @0, @"limit": @200, @"sid" : self.service.sessionIdentifier,
            @"view_mode": self.articlesVisibility == MGBArticleVisibilityAll ? @"all_articles" : @"unread"};

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    self.articleRequest = request;

    [request setCompletionBlock:^{
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[self.articleRequest responseData] options:0 error:nil];

        self.articleRequest = nil;

        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            NSArray *content = [responseDict objectForKey:@"content"];

            callback(content.count);
        } else {
            NSDictionary *content = [responseDict objectForKey:@"content"];

            if ([(NSString *)[content objectForKey:@"error"] isEqualToString:@"NOT_LOGGED_IN"] && perform) {
                // nie zalogowano - trzeba się ponownie zalogować z jeszcze raz wywołać pobieranie nagłówka
                dispatch_async(dispatch_get_global_queue(0, 0), ^{
                    // wątek w tle
                    if ([self.service performLogin]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // wątek główny
                            [self countOffset:callback performLoginIfNecessary:NO];
                        });
                    }
                });
            } else {
                callback(0);
            }
        }
    }];

    [request setFailedBlock:^{
        self.articleRequest = nil;

        callback(0);
    }];

    [request startAsynchronous];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)markAllAsRead:(void (^)(void))callback
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self markAllAsRead:callback performLoginIfNecessary:YES];
    });
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)markAllAsRead:(void (^)(void))callback performLoginIfNecessary:(BOOL)perform
{
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.service.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSDictionary *opDict = @{@"op" : @"catchupFeed", @"sid" : self.service.sessionIdentifier,
            @"cat_id" : [NSNumber numberWithBool:self.isCategory], @"feed_id": [NSNumber numberWithInteger:self.feedID]};
    NSData *jsonData     = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    [request startSynchronous];

    if (![request error]) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];

        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                callback();
            });
        } else {
            NSDictionary *content = [responseDict objectForKey:@"content"];

            if ([(NSString *)[content objectForKey:@"error"] isEqualToString:@"NOT_LOGGED_IN"]) {
                if (perform) {
                    if ([self.service performLogin]) {
                        [self markAllAsRead:callback performLoginIfNecessary:NO];
                    } else {
                        [self.service errorNotLoggedIn];
                    }
                } else {
                    [self.service errorNotLoggedIn];
                }
            } else {
                [self.service errorTTRSS:[content objectForKey:@"error"]];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                callback();
            });
        }
    } else {
        [self.service errorConnection:[request error]];

        dispatch_async(dispatch_get_main_queue(), ^{
            callback();
        });
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)unsubscribe:(void (^)(void))callback
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self unsubscribe:callback performLoginIfNecessary:YES];
    });
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)unsubscribe:(void (^)(void))callback performLoginIfNecessary:(BOOL)perform
{
    if (self.articleRequest.isExecuting) {
        [self.articleRequest cancel];
        self.articleRequest = nil;
    }

    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.service.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSDictionary *opDict = @{@"op" : @"unsubscribeFeed", @"feed_id" : [NSNumber numberWithInteger:self.feedID], @"sid" : self.service.sessionIdentifier};
    NSData *jsonData     = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    [request startSynchronous];

    if (![request error]) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];

        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                callback();
            });
        } else {
            NSDictionary *content = [responseDict objectForKey:@"content"];

            if ([(NSString *)[content objectForKey:@"error"] isEqualToString:@"NOT_LOGGED_IN"] && perform) {
                // nie zalogowano - trzeba się ponownie zalogować z jeszcze raz wywołać pobieranie nagłówka
                if ([self.service performLogin]) {
                    [self unsubscribe:callback performLoginIfNecessary:NO];
                } else {
                    [self.service errorNotLoggedIn];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        callback();
                    });
                }
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.service errorTTRSS:[content objectForKey:@"error"]];
                    callback();
                });
            }
        }
    } else {
        [self.service errorConnection:[request error]];
        dispatch_async(dispatch_get_main_queue(), ^{
            callback();
        });
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)cancelArticleDownload
{
    [self.articleRequest cancel];
    self.articleRequest = nil;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)updateDataFromFeed:(TinyTinyRSSFeed *)feed
{
    if (self.feedID > 0) {
        // zmiana nazwy niedostępna dla feedów specjalnych
        self.name = feed.name;
    }

    self.lastUpdate  = feed.lastUpdate;
    self.hasFeedIcon = feed.hasFeedIcon;
    self.categoryID  = feed.categoryID;
    self.onTheServer = YES;
    self.orderID     = feed.orderID;

    if (self.isCategory) {
        // jeśli to kategoria to trzeba wyzerować ostatnią aktualizację z serwera
        self.lastServerUpdate = -1;
    } else if (self.lastServerUpdate == self.lastUpdate && self.unread != feed.unread) {
        // jeśli po aktualizacji zgadzają się daty ale nie zgadza się liczba nieprzeczytanych wiadomości to naleźy wymusić
        // odświeżanie
        self.lastServerUpdate = -1;
    }

    self.unread = feed.unread;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)decreaseUnreadCount
{
    if (self.unread > 0) {
        self.unread = self.unread - 1;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)increaseUnreadCount
{
    self.unread = self.unread + 1;
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSString *)description
{
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];
    [description appendFormat:@"%@, DB:%ld FEED:%ld, CAT:%ld, UNREAD:%ld, UPDATE:%ld", self.name, self.dbID, self.feedID, self.categoryID, self.unread, self.lastUpdate];
    [description appendString:@">"];
    return description;
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)isEqualTo:(id)other
{
    if ([other isKindOfClass:[self class]]) {
        TinyTinyRSSFeed *otherFeed = (TinyTinyRSSFeed *)other;

        return self.feedID == otherFeed.feedID && self.isCategory == otherFeed.isCategory && self.service == otherFeed.service;
    }

    return NO;
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)isEqual:(id)object
{
    if ([object class] == [self class]) {
        TinyTinyRSSFeed *otherFeed = (TinyTinyRSSFeed *)object;

        return self.feedID == otherFeed.feedID && self.isCategory == otherFeed.isCategory && self.service == otherFeed.service;
    }

    return NO;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)syncWithDatabase
{
    [MicroRSSFeedTable updateTTRSSFeed:self];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)removeLocalData
{
    if ([self.feeds count] > 0) {
        for (TinyTinyRSSFeed *feed in self.feeds) {
            [feed removeLocalData];
        }
    }

    // kasowanie ikonki
    NSString *feedIcon         = [[NSString alloc] initWithFormat:@"%ld.ico", self.feedID];
    NSURL *iconPath            = [[self.service iconsPath] URLByAppendingPathComponent:feedIcon];
    NSFileManager* fileManager = [NSFileManager defaultManager];

    if ([fileManager fileExistsAtPath:[iconPath path]]) {
        [fileManager removeItemAtPath:[iconPath path] error:nil];
    }

    // kasowanie danych z bazy
    [MicroRSSFeedTable removeFeedFromDatabase:self.dbID];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)removeFeedsNotOnServer
{
    NSMutableArray *feedsToRemove = [[NSMutableArray alloc] init];

    for (TinyTinyRSSFeed *feed in self.feeds) {
        if (!feed.onTheServer) {
            // feed lub kategoria nie znajduje się na serwerze usuwamy
            [feedsToRemove addObject:feed];
            [feed removeLocalData];
            feed.categoryFeed = nil;

            NSMutableDictionary *indexDictionary = feed.isCategory ? self.service.categoryIndex : self.service.feedsIndex;
            NSNumber *feedID = [[NSNumber alloc] initWithInteger:feed.feedID];

            TinyTinyRSSFeed *indexedFeed = [indexDictionary objectForKey:feedID];
            if (indexedFeed == feed) {
                [indexDictionary removeObjectForKey:feedID];
            }
        }
    }

    [self.feeds removeObjectsInArray:feedsToRemove];
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark - implementacja DataServiceTreeItem
- (BOOL)hasBadge
{
    return self.unread > 0;
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSInteger)badgeValue
{
    return self.unread;
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSString *)title
{
    return self.name;
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)hasIcon
{
    return self.feedIcon ? YES : NO;
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSImage *)icon
{
    return self.feedIcon;
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSUInteger)numberOfChildren
{
    return [self.visibleFeeds count];
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSArray *)children
{
    return self.visibleFeeds;
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)isExpandable
{
    return [self.visibleFeeds count] > 0;
}

@end