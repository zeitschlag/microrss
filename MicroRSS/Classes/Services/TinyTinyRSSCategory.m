//
// Created by Grzesiek on 27.05.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import "TinyTinyRSSCategory.h"


@implementation TinyTinyRSSCategory

// ---------------------------------------------------------------------------------------------------------------------
- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        self.name       = [dictionary objectForKey:@"title"];
        self.categoryID = [dictionary objectForKey:@"id"];
        self.unread     = [dictionary objectForKey:@"unread"];
        self.feeds      = [[NSMutableArray alloc] init];
    }

    return self;
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSString *)description
{
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];
    [description appendFormat:@"%@, %@, %@", self.name, self.categoryID, self.unread];
    [description appendString:@">"];
    [description appendFormat:@"%@", self.feeds];
    return description;
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)hasBadge
{
    return [self.unread integerValue] > 0;
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSInteger)badgeValue
{
    return [self.unread integerValue];
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSString *)title
{
    return self.name;
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSUInteger)numberOfChildren
{
    return [self.feeds count];
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSArray *)children
{
    return self.feeds;
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)isExpandable
{
    return [self.feeds count] > 0;
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)hasIcon
{
    return YES;
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSImage *)icon
{
    return [[NSWorkspace sharedWorkspace] iconForFileType:NSFileTypeForHFSTypeCode(kGenericFolderIcon)];
}


@end