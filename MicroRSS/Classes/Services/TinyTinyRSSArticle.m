//
// Created by Grzesiek on 23.06.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <FMDB/FMResultSet.h>
#import "TinyTinyRSSArticle.h"
#import "TinyTinyRSSFeed.h"
#import "ASIHTTPRequest.h"
#import "TinyTinyRSS.h"
#import "NSString+HTML.h"
#import "MicroRSSArticleTable.h"


@interface TinyTinyRSSArticle ()

// Oznacza artykuł jako
- (void)markArticle:(NSNumber *)mode as:(NSNumber *)field performLoginIfNecessary:(BOOL)login;
@end

@implementation TinyTinyRSSArticle

// ---------------------------------------------------------------------------------------------------------------------
- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];

    if (self) {
        self.feedID       = [[dictionary objectForKey:@"feed_id"] integerValue];
        self.articleID    = [[dictionary objectForKey:@"id"] integerValue];
        self.articleTitle = [[[dictionary objectForKey:@"title"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] stringByDecodingHTMLEntities];
        self.unread       = [(NSNumber *)[dictionary objectForKey:@"unread"] boolValue];
        self.starred      = [(NSNumber *)[dictionary objectForKey:@"marked"] boolValue];
        self.published    = [(NSNumber *)[dictionary objectForKey:@"published"] boolValue];
        self.link         = [dictionary objectForKey:@"link"];
        if ([dictionary objectForKey:@"excerpt"]) {
            self.excerpt      = [[[[dictionary objectForKey:@"excerpt"] stringByReplacingOccurrencesOfString:@"\n" withString:@" "] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] stringByDecodingHTMLEntities];
        } else {
            NSString *excerpt = [[dictionary objectForKey:@"content"] stringByConvertingHTMLToPlainText];
            if (excerpt.length > 160) {
                excerpt = [excerpt substringToIndex:160];
            }
            self.excerpt = excerpt;
        }
        self.content      = [dictionary objectForKey:@"content"];
        self.updated      = [[dictionary objectForKey:@"updated"] integerValue];
    }

    return self;
}

// ---------------------------------------------------------------------------------------------------------------------
- (id)initWithResultSet:(FMResultSet *)resultSet
{
    self = [super init];

    if (self) {
        self.dbID         = [resultSet intForColumn:@"id"];
        self.feedID       = [resultSet intForColumn:@"feed_id"];
        self.articleID    = [resultSet intForColumn:@"article_id"];
        self.articleTitle = [resultSet stringForColumn:@"title"];
        self.unread       = [resultSet boolForColumn:@"unread"];
        self.starred      = [resultSet boolForColumn:@"starred"];
        self.published    = [resultSet boolForColumn:@"published"];
        self.link         = [resultSet stringForColumn:@"link"];
        self.excerpt      = [resultSet stringForColumn:@"excerpt"];
        self.content      = [resultSet stringForColumn:@"content"];
        self.updated      = [resultSet intForColumn:@"updated"];
    }

    return self;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)markAsUnread:(BOOL)unread
{
    self.unread = unread;

    if (unread) {
        [self markArticle:@1 as:@2 performLoginIfNecessary:YES];
        [self.feed increaseUnreadCount];
        [self.feed.service.allFeed increaseUnreadCount];
        [self.feed.categoryFeed increaseUnreadCount];

        if (self.starred) {
            [self.feed.service.starredFeed increaseUnreadCount];
        }

        // określanie czy dany artykuł jest świeży
        NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
        if (self.feed.service.freshArticleMaxAge * 60 * 60 > timeStamp - self.updated) {
            [self.feed.service.freshFeed increaseUnreadCount]; // jeśli artykuł jest świeży to aktualizujemy również licznik świeżych
        }
    } else {
        [self markArticle:@0 as:@2 performLoginIfNecessary:YES];
        [self.feed decreaseUnreadCount];
        [self.feed.service.allFeed decreaseUnreadCount];
        [self.feed.categoryFeed decreaseUnreadCount];

        if (self.starred) {
            [self.feed.service.starredFeed decreaseUnreadCount];
        }

        // określanie czy dany artykuł jest świeży
        NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
        if (self.feed.service.freshArticleMaxAge * 60 * 60 > timeStamp - self.updated) {
            [self.feed.service.freshFeed decreaseUnreadCount]; // jeśli artykuł jest świeży to aktualizujemy również licznik świeżych
        }
    }

    [MicroRSSArticleTable markArticle:self.articleID forService:self.feed.service.serviceID asUnread:unread];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)markAsStarred:(bool)starred
{
    self.starred = starred;

    [self markArticle:(starred ? @1 : @0) as:@0 performLoginIfNecessary:YES];

    if (self.unread) {
        if (starred) {
            [self.feed.service.starredFeed increaseUnreadCount];
        } else {
            [self.feed.service.starredFeed decreaseUnreadCount];
        }
    }

    self.feed.service.starredFeed.lastServerUpdate = -1; // wymuszanie aktualizacji

    [MicroRSSArticleTable markArticle:self.articleID forService:self.feed.service.serviceID asStarred:starred];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)markAsPublished:(bool)published
{
    self.published = published;

    [self markArticle:(published ? @1 : @0) as:@1 performLoginIfNecessary:YES];

    if (self.unread) {
        if (published) {
            [self.feed.service.publishedFeed increaseUnreadCount];
        } else {
            [self.feed.service.publishedFeed decreaseUnreadCount];
        }
    }

    self.feed.service.publishedFeed.lastServerUpdate = -1; // wymuszenie aktualizacji

    [MicroRSSArticleTable markArticle:self.articleID forService:self.feed.service.serviceID asPublished:published];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)markArticle:(NSNumber *)mode as:(NSNumber *)field performLoginIfNecessary:(BOOL)login
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSDictionary *opDict = @{@"op" : @"updateArticle", @"article_ids": [NSNumber numberWithInteger:self.articleID], @"sid" : self.feed.service.sessionIdentifier, @"mode": mode, @"field": field};
        NSData *jsonData     = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

        ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.feed.service.apiURL];
        [request setUseCookiePersistence:NO];
        [request addRequestHeader:@"Accept" value:@"*/*"];
        [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
        [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

        [request appendPostData:jsonData];

        [request startSynchronous];

        if (![request error]) {
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];

            NSNumber *status = [responseDict objectForKey:@"status"];

            if (status.integerValue == 0) {
                NSDictionary *content = [responseDict objectForKey:@"content"];

                if ([(NSString *)[content objectForKey:@"status"] isEqualToString:@"OK"]) {
                    // wszystko pobrano OK
                }
            } else {
                NSDictionary *content = [responseDict objectForKey:@"content"];

                if ([(NSString *)[content objectForKey:@"error"] isEqualToString:@"NOT_LOGGED_IN"] && login) {
                    // nie zalogowano - trzeba się ponownie zalogować i jeszcze raz wywołać oznaczanie artykułu
                    dispatch_async(dispatch_get_global_queue(0, 0), ^{
                        // wątek w tle
                        if ([self.feed.service performLogin]) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                // wątek główny
                                [self markArticle:mode as:field performLoginIfNecessary:NO];
                            });
                        } else {
                            [self.feed.service errorNotLoggedIn];
                        }
                    });
                } else {
                    [self.feed.service errorTTRSS:[content objectForKey:@"error"]];
                }
            }
        } else {
            [self.feed.service errorConnection:[request error]];
        }
    });
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSString *)updateTimeString
{
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[[NSDate alloc] init]];

    NSDate *now  = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:self.updated];

    NSTimeInterval elapsed = [now timeIntervalSinceDate:date];

    if (elapsed < 0) {
        return NSLocalizedString(@"today", @"dzisiaj");
    } else if (elapsed < 86400) {
        return NSLocalizedString(@"yesterday", @"wczoraj");
    } else {
        return [NSString stringWithFormat:NSLocalizedString(@"%ld days ago", @"%d dni temu"), (NSInteger)(elapsed / 86400) + 1];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSString *)updateTimeStringFull
{
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:self.updated];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

    [formatter setDateFormat:@"YYYY-MM-dd HH:mm"];

    return [formatter stringFromDate:date];
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSString *)description
{
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];
    [description appendFormat:@"%ld, %@, %d, %@", self.articleID, self.articleTitle, self.unread, self.link];
    [description appendString:@">"];
    return description;
}

// ---------------------------------------------------------------------------------------------------------------------

@end