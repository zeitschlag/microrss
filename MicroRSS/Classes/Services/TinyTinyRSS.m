//
// Created by Grzesiek on 26.05.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <sys/socket.h>
#import "TinyTinyRSS.h"
#import "ASIHTTPRequest.h"
#import "DataServiceDelegate.h"
#import "TinyTinyRSSFeed.h"
#import "MicroRSSFeedTable.h"
#import "MGBErrorDomain.h"
#import "MicroRSSErrorCode.h"
#import "MicroRSSServiceTable.h"

static unsigned char staticFeedsVisibility = MGB_FEEDS_VISIBILITY_ALL;

static BOOL staticAcceptSelfSignedCert = NO;


@interface TinyTinyRSS ()
// Metoda wywoływana w tle aby wykonać całą aktualizację danych
- (void)performDataUpdateInBackground;

// Metoda sprawdza czy jesteśmy poprawnie zalogowani do serwisu
- (BOOL)isLoggedIn;

// Pobiera listę kategorii
- (BOOL)downloadCategories;

// Wykonuje aktualizacje feedów
- (void)updateFeeds;

// Oznacza feed'y jako nieobecne na serwerze
- (void)markFeedsAsNotOnServer;

// Kasuje feed'y które nie znajdują się na serwerze
- (void)removeFeedsNotOnServer;

// Pobiera na dysk ikonkę dla danego feed'a
- (void)downloadIconForFeed:(TinyTinyRSSFeed *)feed;

// Wczytuje ikonę feed'a z dysku
- (BOOL)loadFeedIconFromDisk:(TinyTinyRSSFeed *)feed;

// Pobiera konfigurację dla danego serwisu
- (void)downloadConfig;

// Pobiera wartość z preferencji
- (id)downloadPreferenceValue:(NSString *)prefName;

// Subskrybuje feed przeprowadzając operacje logowania jeśli zachodzi taka potrzeba
- (void)subscribeToFeed:(NSString *)feedURL forCategory:(NSInteger)categoryID callback:(void (^)(NSError *error))callback performLoginIfNecessary:(BOOL)perform;

// Numer ID sesji
@property (nonatomic, retain) NSString *sessionID;

// Adres URL z którego będą pobierane ikonki
@property (nonatomic, retain) NSURL *iconsURL;

// Ścieżka do katalogu na dysku gdzie przechowywane są ikonki
@property (nonatomic, retain) NSURL *iconsDir;

// Tablica z listą feed'ów
@property (nonatomic, retain) NSMutableArray *feedsArray;

// Maksymalny czas "życia" świeżych artykułów
@property (nonatomic, assign, readwrite) NSInteger freshArticleMaxAge;

// Czy dany serwis posiada jakieś niezapisane dane
@property (nonatomic, assign) BOOL isDirty;
@end

@implementation TinyTinyRSS
// ---------------------------------------------------------------------------------------------------------------------
- (id)init
{
    self = [super init];
    if (self) {
        self.categoryIndex = [[NSMutableDictionary alloc] init];
        self.feedsIndex    = [[NSMutableDictionary alloc] init];

        self.sessionID  = @"";
        self.feedsArray = [[NSMutableArray alloc] init];

        self.freshArticleMaxAge = -1;

        // Dodawanie feedów specjalnych

        // Wszystkie artykuły
        TinyTinyRSSFeed *all = [[TinyTinyRSSFeed alloc] initWithDictionary:@{
                @"id"     : @-4,
                @"title"  : NSLocalizedString(@"All articles", @"Wszystkie artykuły"),
                @"unread" : @0
        }];
        all.service = self;

        [self.feedsArray addObject:all];
        [self.feedsIndex setObject:all forKey:@-4];
        self.allFeed = all;

        // Świeże artykuły
        TinyTinyRSSFeed *fresh = [[TinyTinyRSSFeed alloc] initWithDictionary:@{
                @"id"     : @-3,
                @"title"  : NSLocalizedString(@"Fresh articles", @"Świeże artykuły"),
                @"unread" : @0
        }];
        fresh.service = self;

        [self.feedsArray addObject:fresh];
        [self.feedsIndex setObject:fresh forKey:@-3];
        self.freshFeed = fresh;

        // Oznaczone gwiazdką
        TinyTinyRSSFeed *starred = [[TinyTinyRSSFeed alloc] initWithDictionary:@{
                @"id"     : @-1,
                @"title"  : NSLocalizedString(@"Starred articles", @"Oznaczone gwiazdką"),
                @"unread" : @0
        }];
        starred.service = self;

        [self.feedsArray addObject:starred];
        [self.feedsIndex setObject:starred forKey:@-1];
        self.starredFeed = starred;

        // Opublikowane artykuły
        TinyTinyRSSFeed *published = [[TinyTinyRSSFeed alloc] initWithDictionary:@{
                @"id"     : @-2,
                @"title"  : NSLocalizedString(@"Published articles", @"Opublikowane artykuły"),
                @"unread" : @0
        }];
        published.service = self;

        [self.feedsArray addObject:published];
        [self.feedsIndex setObject:published forKey:@-2];
        self.publishedFeed = published;

        // Zarchiwizowane artykuły
        TinyTinyRSSFeed *archived = [[TinyTinyRSSFeed alloc] initWithDictionary:@{
                @"id"     : @0,
                @"title"  : NSLocalizedString(@"Archived articles", @"Zarchiwizowane artykuły"),
                @"unread" : @0
        }];
        archived.service = self;

        [self.feedsArray addObject:archived];
        [self.feedsIndex setObject:archived forKey:@0];

        // Ostatnio czytane
        TinyTinyRSSFeed *recentlyRead = [[TinyTinyRSSFeed alloc] initWithDictionary:@{
                @"id"     : @-6,
                @"title"  : NSLocalizedString(@"Recently read", @"Ostatnio czytane"),
                @"unread" : @0
        }];
        recentlyRead.service = self;

        [self.feedsArray addObject:recentlyRead];
        [self.feedsIndex setObject:recentlyRead forKey:@-6];
    }

    return self;
}

- (id)initWithApiURL:(NSURL *)apiURL delegate:(id <DataServiceDelegate>)delegate login:(NSString *)login password:(NSString *)password name:(NSString *)name
{
    self = [self init];
    if (self) {
        self.apiURL   = apiURL;
        self.delegate = delegate;
        self.login    = login;
        self.password = password;
        self.name     = name;

        self.isDirty = NO;
    }

    return self;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)initializeServiceFromDatabase
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self loadFeedsFromDatabase];

        // po wczytaniu z bazy powiadamianie delegata na wątku głównym
        dispatch_async(dispatch_get_main_queue(), ^{
            self.feeds = self.feedsArray;

            [self.delegate performSelector:@selector(dataUpdatedForService:) withObject:self];
        });

        // wczytywanie danych z serwera
        [self performDataUpdateInBackground];
    });
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)performDataUpdate
{
    self.dataLoaded = NO;

    [self performSelectorInBackground:@selector(performDataUpdateInBackground) withObject:nil];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)performDataUpdateInBackground
{
    if (![self isLoggedIn]) {
        // nie jesteśmy zalogowani - trzeba się zalogować
        if (![self performLogin]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.dataLoaded = YES;
                [self errorNotLoggedIn];

                [self.delegate performSelector:@selector(dataUpdatedForService:) withObject:self];
            });

            return;
        }
    }

    [self updateFeeds];

}

// ---------------------------------------------------------------------------------------------------------------------
- (void)errorNotLoggedIn
{
    if ([self.delegate respondsToSelector:@selector(errorOccurred:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // wątek główny
            NSError *error = [MGBErrorDomain errorWithErrorCode:MicroRSSErrorCodeNotLoggedIn userInfo:@{
                    NSLocalizedDescriptionKey: NSLocalizedString(@"Unable to login.", @"Unable to login."),
                    @"MGMCreatedAt": [NSDate date]
            }];

            [self.delegate errorOccurred:error];
        });
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)errorTTRSS:(NSString *)errorMessage
{
    if ([self.delegate respondsToSelector:@selector(errorOccurred:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // wątek główny
            NSError *error = [MGBErrorDomain errorWithErrorCode:MicroRSSErrorCodeTTRSSError userInfo:@{
                    NSLocalizedDescriptionKey: [NSString stringWithFormat:NSLocalizedString(@"Tiny Tiny RSS responded with an error: %@", @"Tiny Tiny RSS responded with an error: %@"), errorMessage],
                    @"MGMCreatedAt": [NSDate date]
            }];

            [self.delegate errorOccurred:error];
        });
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)errorConnection:(NSError *)connectionError
{
    if ([self.delegate respondsToSelector:@selector(errorOccurred:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // wątek główny
            NSError *error;
            
            if (connectionError) {
                error = [MGBErrorDomain errorWithErrorCode:MicroRSSErrorCodeConnectionError userInfo:@{
                        NSLocalizedDescriptionKey: [connectionError localizedDescription] ? [connectionError localizedDescription] : [connectionError description],
                        @"MGMCreatedAt": [NSDate date]
                }];

                [self.delegate errorOccurred:error];
            } else {
//                error = [MGBErrorDomain errorWithErrorCode:MicroRSSErrorCodeConnectionError userInfo:@{
//                        NSLocalizedDescriptionKey: @"Unknown connection error", @"MGMCreatedAt": [NSDate date]}];
            }

            //[self.delegate errorOccurred:error];
        });
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)loadFeedsFromDatabase
{
    NSArray *feedsArray = [MicroRSSFeedTable fetchTTRSSFeedsForService:self.serviceID];

    for (NSDictionary *data in feedsArray) {
        if ([(NSNumber *)[data objectForKey:@"is_category"] boolValue]) {
            // dodajemy kategorię
            TinyTinyRSSFeed *category = [[TinyTinyRSSFeed alloc] initWithDatabaseCategoryDictionary:data];
            category.service          = self;

            NSNumber *catID = [[NSNumber alloc] initWithInteger:category.feedID];
            [self.categoryIndex setObject:category forKey:catID];
            [self.feedsArray addObject:category];
        } else {
            // dodajemy feed sprawdzając wcześniej czy taki już nie istnieje w pamięci (jeśli istnieje jest aktualizowany)
            TinyTinyRSSFeed *feed = [[TinyTinyRSSFeed alloc] initWithDatabaseDictionary:data];

            NSNumber *categoryID = [[NSNumber alloc] initWithInteger:feed.categoryID];

            if (categoryID.integerValue == 0) {
                NSNumber *feedID = [[NSNumber alloc] initWithInteger:feed.feedID];

                TinyTinyRSSFeed *existingFeed = [self.feedsIndex objectForKey:feedID];
                if (existingFeed) {
                    [existingFeed updateDataFromFeed:feed];
                    existingFeed.articlesSortOrder  = feed.articlesSortOrder;
                    existingFeed.articlesVisibility = feed.articlesVisibility;
                    existingFeed.dbID               = feed.dbID;
                } else {
                    feed.service = self;
                    [self.feedsIndex setObject:feed forKey:feedID];
                    [self.feedsArray addObject:feed];

                    if (feed.hasFeedIcon && !feed.icon) {
                        [self loadFeedIconFromDisk:feed];
                    }
                }
            } else {
                if (categoryID.integerValue == -1) {
                    // feedy specjalne
                    TinyTinyRSSFeed *specialFeed = [self.feedsIndex objectForKey:[NSNumber numberWithInteger:feed.feedID]];
                    [specialFeed updateDataFromFeed:feed];
                    specialFeed.articlesSortOrder  = feed.articlesSortOrder;
                    specialFeed.articlesVisibility = feed.articlesVisibility;
                    specialFeed.dbID               = feed.dbID;
                } else {
                    // trzeba wyszukać jego kategorię i do niej dodać element
                    TinyTinyRSSFeed *category = [self.categoryIndex objectForKey:categoryID];

                    if (category) {
                        if ([category addFeed:feed]) {
                            feed.service = self;
                            [self.feedsIndex setObject:feed forKey:[NSNumber numberWithInteger:feed.feedID]];

                            if (feed.hasFeedIcon && !feed.icon) {
                                [self loadFeedIconFromDisk:feed];
                            }
                        }
                    }
                }
            }
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)isLoggedIn
{
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSDictionary *opDict = @{@"sid" : self.sessionID, @"op" : @"isLoggedIn"};
    NSData *jsonData     = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    [request startSynchronous];

    if (![request error]) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];

        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            NSDictionary *content   = [responseDict objectForKey:@"content"];
            NSNumber *contentStatus = [content objectForKey:@"status"];

            return [contentStatus boolValue];
        }
    } else {
        [self errorConnection:[request error]];
    }

    return NO;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)testConnection:(void (^)(BOOL))callback
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        BOOL ok = [self performLogin];

        dispatch_async(dispatch_get_main_queue(), ^{
            callback(ok);
        });
    });
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)performLogin
{
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSDictionary *opDict = @{@"op" : @"login", @"user" : self.login, @"password" : self.password};
    NSData *jsonData     = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    [request startSynchronous];

    if (![request error]) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];
        
        if (!responseDict) {
            return NO;
        }

        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            NSDictionary *content = [responseDict objectForKey:@"content"];

            self.sessionID = [content objectForKey:@"session_id"];
            if (!self.sessionID) {
                self.sessionID = @"";
                return NO;
            }

            return YES;
        }
    } else {
        NSLog(@"%@", [request error]);
    }

    return NO;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)performLogout
{
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSDictionary *opDict = @{@"op" : @"logout", @"sid" : self.sessionID};
    NSData *jsonData     = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    [request startSynchronous];

    if (![request error]) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];

        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            self.sessionID = @"";
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)downloadCategories
{
    if (!self.sessionID) {
        return NO;
    }
    
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSDictionary *opDict = @{@"op" : @"getCategories", @"sid" : self.sessionID};
    NSData *jsonData     = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    [request startSynchronous];

    if (![request error]) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];

        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            NSArray *content = [responseDict objectForKey:@"content"];

            for (NSDictionary *category in content) {
                NSNumber *catID = [[NSNumber alloc] initWithInteger:[[category objectForKey:@"id"] integerValue]];

                if (catID.integerValue > 0) { // nie pobieramy kategorii specjalnych
                    TinyTinyRSSFeed *categoryModel = [[TinyTinyRSSFeed alloc] initWithCategoryDictionary:category];
                    categoryModel.service = self;

                    TinyTinyRSSFeed *existingCategory = [self.categoryIndex objectForKey:catID];

                    if (existingCategory) {
                        // kategoria istnieje - należy zaktualizować
                        [existingCategory updateDataFromFeed:categoryModel];
                    } else {
                        // dodajemy nową kategorię
                        [self.feedsArray addObject:categoryModel];
                        [self.categoryIndex setObject:categoryModel forKey:catID];
                    }
                }
            }

            return YES;
        } else {
            NSDictionary *content = [responseDict objectForKey:@"content"];

            if ([(NSString *)[content objectForKey:@"error"] isEqualToString:@"NOT_LOGGED_IN"]) {
                [self errorNotLoggedIn];
            } else {
                [self errorTTRSS:[content objectForKey:@"error"]];
            }
        }
    } else {
        [self errorConnection:[request error]];
    }

    return NO;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)updateFeeds
{    
    [self markFeedsAsNotOnServer];

    if (![self downloadCategories]) {
        // wystąpił błąd pobierania kategorii - operacja przerwana
        return;
    }

    // Pobieranie listy feed'ów
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSDictionary *opDict = @{@"op" : @"getFeeds", @"sid" : self.sessionID, @"cat_id" : @-4};
    NSData *jsonData     = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    [request startSynchronous];

    if (![request error]) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];

        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            // przed dodaniem nowych feed'ów czyszczenie widocznych feed'ów w kategoriach
            for (NSNumber *categoryID in self.categoryIndex) {
                TinyTinyRSSFeed *category = [self.categoryIndex objectForKey:categoryID];
                [category clearVisibleFeeds];
            }

            NSArray *content = [responseDict objectForKey:@"content"];

            for (NSDictionary *feedData in content) {
                TinyTinyRSSFeed *feed = [[TinyTinyRSSFeed alloc] initWithDictionary:feedData];
                feed.service = self;

                NSNumber *categoryID = [feedData objectForKey:@"cat_id"];

                if (categoryID.integerValue == 0) {
                    // jeśli brak kategorii dodajemy bezpośrednio do drzewka
                    NSUInteger feedIndex = [self.feedsArray indexOfObject:feed];
                    if (feedIndex != NSNotFound) {
                        // znaleziono feed'a należy zaktualizować
                        TinyTinyRSSFeed *existingFeed = [self.feedsArray objectAtIndex:feedIndex];
                        [existingFeed updateDataFromFeed:feed];
                        feed = existingFeed;
                    } else {
                        // dodajemy nowego feed'a
                        [self.feedsArray addObject:feed];
                        [self.feedsIndex setObject:feed forKey:[NSNumber numberWithInteger:feed.feedID]];
                    }
                } else {
                    if (categoryID.integerValue == -1) {
                        // feedy specjalne
                        TinyTinyRSSFeed *specialFeed = [self.feedsIndex objectForKey:[NSNumber numberWithInteger:feed.feedID]];
                        [specialFeed updateDataFromFeed:feed];
                    } else {
                        // trzeba wyszukać jego kategorię i do niej dodać element
                        // numer id jest tworzony ponownie aby zapewnić zgodność podczas przeszukiwaniwa
                        NSNumber *categoryIDKey   = [[NSNumber alloc] initWithInteger:categoryID.integerValue];
                        TinyTinyRSSFeed *category = [self.categoryIndex objectForKey:categoryIDKey];

                        if (category) {
                            if ([category addFeed:feed]) {
                                [self.feedsIndex setObject:feed forKey:[NSNumber numberWithInteger:feed.feedID]];
                            }
                        }
                    }
                }

                if (feed.hasFeedIcon && !feed.icon) {
                    [self downloadIconForFeed:feed];
                }
            }
        } else {
            NSDictionary *content = [responseDict objectForKey:@"content"];

            if ([(NSString *)[content objectForKey:@"error"] isEqualToString:@"NOT_LOGGED_IN"]) {
                [self errorNotLoggedIn];
            } else {
                [self errorTTRSS:[content objectForKey:@"error"]];
            }
            return;
        }
    } else {
        [self errorConnection:[request error]];
        return;
    }

    [self removeFeedsNotOnServer];

    [self syncFeedsWithDatabase];

    if (self.freshArticleMaxAge == -1) {
        NSNumber *freshMaxAge   = [self downloadPreferenceValue:@"FRESH_ARTICLE_MAX_AGE"];
        self.freshArticleMaxAge = [freshMaxAge integerValue];
    }

    // zapamiętywanie wyniku odbywa się na wątku głównym aby nie kolidować z procesem odczytującym dane
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([TinyTinyRSS feedsVisibility] == MGB_FEEDS_VISIBILITY_UNREAD) {
            NSMutableArray *unreadFeedsArray = [[NSMutableArray alloc] init];
            for (TinyTinyRSSFeed *feed in self.feedsArray) {
                if (feed.unread > 0 || feed.categoryID == -1) {
                    // dodajemy tylko nieprzeczytane oraz pochodzące z kategorii specjalnej
                    [unreadFeedsArray addObject:feed];
                }
            }
            self.feeds = unreadFeedsArray;
        } else {
            self.feeds = self.feedsArray;
        }

        self.dataLoaded = YES;

        [self.delegate performSelector:@selector(dataUpdatedForService:) withObject:self];
    });
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)syncFeedsWithDatabase
{
    NSMutableArray *totalFeedsArray = [[NSMutableArray alloc] initWithCapacity:[self.feedsIndex count] + [self.categoryIndex count]];

    for (NSNumber *key in self.feedsIndex) {
        [totalFeedsArray addObject:[self.feedsIndex objectForKey:key]];
    }

    for (NSNumber *key in self.categoryIndex) {
        [totalFeedsArray addObject:[self.categoryIndex objectForKey:key]];
    }

    [MicroRSSFeedTable updateTTRSSFeeds:totalFeedsArray];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)markFeedsAsNotOnServer
{
// Ustawianie feedów jako nieobecnych na serwerze aby później wiedzieć które należy usunąć
    for (NSNumber *key in self.feedsIndex) {
        [(TinyTinyRSSFeed *)[self.feedsIndex objectForKey:key] setOnTheServer:NO];
    }

    for (NSNumber *key in self.categoryIndex) {
        [(TinyTinyRSSFeed *)[self.categoryIndex objectForKey:key] setOnTheServer:NO];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)removeFeedsNotOnServer
{
    NSMutableArray *feedsToRemove = [[NSMutableArray alloc] init];

    for (TinyTinyRSSFeed *feed in self.feedsArray) {
        if (!feed.onTheServer) {
            // feed lub kategoria nie znajduje się na serwerze usuwamy
            [feedsToRemove addObject:feed];
            [feed removeLocalData];

            NSMutableDictionary *indexDictionary = feed.isCategory ? self.categoryIndex : self.feedsIndex;
            NSNumber *feedID = [[NSNumber alloc] initWithInteger:feed.feedID];

            TinyTinyRSSFeed *indexedFeed = [indexDictionary objectForKey:feedID];
            if (indexedFeed == feed) {
                [indexDictionary removeObjectForKey:feedID];
            }
        } else if (feed.isCategory) {
            // kategoria - trzeba sprwadzić feedy pod nią
            [feed removeFeedsNotOnServer];
        }
    }

    [self.feedsArray removeObjectsInArray:feedsToRemove];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)downloadIconForFeed: (TinyTinyRSSFeed *)feed
{
    NSString *feedIcon         = [[NSString alloc] initWithFormat:@"%ld.ico", feed.feedID];
    NSURL *iconPath            = [[self iconsPath] URLByAppendingPathComponent:feedIcon];
    NSFileManager* fileManager = [NSFileManager defaultManager];

    if ([fileManager fileExistsAtPath:[iconPath path]]) {
        [feed loadIconFromURL:iconPath];
        return;
    }

    if (!self.iconsURL) {
        // pobieranie konfiguracji aby wiedzieć z jakiego adresu mają zostać pobrane ikony
        [self downloadConfig];
    }

    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[self.iconsURL URLByAppendingPathComponent:feedIcon]];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];
    [request setDownloadDestinationPath:[iconPath path]];
    [request startSynchronous];

    if (![request error]) {
        if ([fileManager fileExistsAtPath:[iconPath path]]) {
            [feed loadIconFromURL:iconPath];
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)loadFeedIconFromDisk:(TinyTinyRSSFeed *)feed
{
    NSString *feedIcon         = [[NSString alloc] initWithFormat:@"%ld.ico", feed.feedID];
    NSURL *iconPath            = [[self iconsPath] URLByAppendingPathComponent:feedIcon];
    NSFileManager* fileManager = [NSFileManager defaultManager];

    if ([fileManager fileExistsAtPath:[iconPath path]]) {
        [feed loadIconFromURL:iconPath];
        return YES;
    }

    return NO;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)downloadConfig
{
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSDictionary *opDict = @{@"op" : @"getConfig", @"sid" : self.sessionID};
    NSData *jsonData     = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    [request startSynchronous];
    if (![request error]) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];

        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            NSDictionary *content = [responseDict objectForKey:@"content"];

            // adres URL do pobierania ikon
            NSString *iconsURLString = [content objectForKey:@"icons_url"];

            NSURL *iconsURL = [[NSURL URLWithString:[@"/" stringByAppendingString:iconsURLString] relativeToURL:self.apiURL] absoluteURL];

            self.iconsURL = iconsURL;
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (id)downloadPreferenceValue:(NSString *)prefName
{
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSDictionary *opDict = @{@"op" : @"getPref", @"sid" : self.sessionID, @"pref_name": prefName};
    NSData *jsonData     = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    [request startSynchronous];
    if (![request error]) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];

        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            NSDictionary *content = [responseDict objectForKey:@"content"];

            return [content objectForKey:@"value"];
        } else {
            return nil;
        }
    } else {
        return nil;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSURL *)iconsPath
{
    if (self.iconsDir) {
        return self.iconsDir;
    }

    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSString* bundleID         = [[NSBundle mainBundle] bundleIdentifier];
    NSArray* urlPaths          = [fileManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask];

    NSURL* appDirectory = [[urlPaths objectAtIndex:0] URLByAppendingPathComponent:bundleID isDirectory:YES];

    if (![fileManager fileExistsAtPath:[appDirectory path]]) {
        [fileManager createDirectoryAtURL:appDirectory withIntermediateDirectories:NO attributes:nil error:nil];
    }

    NSURL *iconsDirectory = [appDirectory URLByAppendingPathComponent:@"icons" isDirectory:YES];
    if (![fileManager fileExistsAtPath:[iconsDirectory path]]) {
        [fileManager createDirectoryAtURL:iconsDirectory withIntermediateDirectories:NO attributes:nil error:nil];
    }

    NSURL *iconsServiceDirectory = [iconsDirectory URLByAppendingPathComponent:[NSString stringWithFormat:@"%ld", self.serviceID] isDirectory:YES];
    if (![fileManager fileExistsAtPath:[iconsServiceDirectory path]]) {
        [fileManager createDirectoryAtURL:iconsServiceDirectory withIntermediateDirectories:NO attributes:nil error:nil];
    }

    self.iconsDir = iconsServiceDirectory;

    return iconsServiceDirectory;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)subscribeToFeed:(NSString *)feedURL forCategory:(NSInteger)categoryID callback:(void (^)(NSError *error))callback
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self subscribeToFeed:feedURL forCategory:categoryID callback:callback performLoginIfNecessary:YES];
    });
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)subscribeToFeed:(NSString *)feedURL forCategory:(NSInteger)categoryID callback:(void (^)(NSError *error))callback performLoginIfNecessary:(BOOL)perform
{
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:self.apiURL];
    [request setUseCookiePersistence:NO];
    [request addRequestHeader:@"Accept" value:@"*/*"];
    [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    [request setValidatesSecureCertificate:![TinyTinyRSS acceptSelfSignedCert]];

    NSDictionary *opDict = @{@"op" : @"subscribeToFeed", @"category_id" : [NSNumber numberWithInteger:categoryID], @"sid" : self.sessionID, @"feed_url" : feedURL};
    NSData *jsonData     = [NSJSONSerialization dataWithJSONObject:opDict options:0 error:nil];

    [request appendPostData:jsonData];
    [request startSynchronous];

    if (![request error]) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[request responseData] options:0 error:nil];

        NSNumber *status = [responseDict objectForKey:@"status"];
        if (status.integerValue == 0) {
            NSDictionary *content = [responseDict objectForKey:@"content"];
            NSDictionary *status  = [content objectForKey:@"status"];
            NSInteger code        = [(NSNumber *)[status objectForKey:@"code"] integerValue];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (code == 0 || code == 1) {
                    callback(nil);
                } else {
                    NSMutableDictionary *errorDetail = [[NSMutableDictionary alloc] initWithCapacity:1];
                    switch (code) {
                        case 2:
                            [errorDetail setObject:NSLocalizedString(@"URL address is invalid.", @"Adres URL jest nieprawidłowy.") forKey:NSLocalizedDescriptionKey];
                            break;
                        case 3:
                            [errorDetail setObject:NSLocalizedString(@"URL content is HTML, no feeds available.", @"Adres URL zawiera nieprawidłowe dane.") forKey:NSLocalizedDescriptionKey];
                            break;
                        case 4:
                            [errorDetail setObject:NSLocalizedString(@"URL content is HTML which contains multiple feeds.", @"Adres URL zawiera wiele dostępnych źródeł danych.")
                                            forKey:NSLocalizedDescriptionKey];
                            break;
                        case 5:
                            [errorDetail setObject:NSLocalizedString(@"Couldn't download the URL content.", @"Nie udało się pobrać zawartości adresu URL.") forKey:NSLocalizedDescriptionKey];
                            break;
                        case 6:
                            [errorDetail setObject:NSLocalizedString(@"Content is an invalid XML.", @"Adres URL zawiera nieprawidłowy XML.") forKey:NSLocalizedDescriptionKey];
                            break;
                    }

                    NSError *error = [NSError errorWithDomain:@"TTRSSSubscribeToFeedDomain" code:code userInfo:errorDetail];
                    callback(error);
                }
            });
        } else {
            NSDictionary *content = [responseDict objectForKey:@"content"];

            if ([(NSString *)[content objectForKey:@"error"] isEqualToString:@"NOT_LOGGED_IN"] && perform) {
                // nie zalogowano - trzeba się ponownie zalogować z jeszcze raz wywołać pobieranie nagłówka
                if ([self performLogin]) {
                    [self subscribeToFeed:feedURL forCategory:categoryID callback:callback performLoginIfNecessary:NO];
                } else {
                    [self errorNotLoggedIn];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSDictionary *errorDetail = @{NSLocalizedDescriptionKey : NSLocalizedString(@"Unable to login.", @"Unable to login.")};
                        NSError *error = [NSError errorWithDomain:@"TTRSSSubscribeToFeedDomain" code:100 userInfo:errorDetail];
                        callback(error);
                    });
                }
            } else {
                [self errorTTRSS:[content objectForKey:@"error"]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSDictionary *errorDetail = @{NSLocalizedDescriptionKey : [content objectForKey:@"error"]};
                    NSError *error = [NSError errorWithDomain:@"TTRSSSubscribeToFeedDomain" code:101 userInfo:errorDetail];
                    callback(error);
                });
            }
        }
    } else {
        [self errorConnection:[request error]];
        dispatch_async(dispatch_get_main_queue(), ^{
            callback([request error]);
        });
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)downloadNewHeadlines:(void (^)(NSArray *))callback
{
    if (self.allFeed.articlesSortOrder == MGBSortingOrderDescending) {
        [self.allFeed downloadNewArticles:callback];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSString *)sessionIdentifier
{
    return self.sessionID;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)setName:(NSString *)name
{
    if (![_name isEqualToString:name]) {
        _name = name;

        self.isDirty = YES;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)setLogin:(NSString *)login
{
    if (![_login isEqualToString:login]) {
        _login = login;

        self.isDirty = YES;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)setPassword:(NSString *)password
{
    if (![_password isEqualToString:password]) {
        _password = password;

        self.isDirty = YES;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)setApiURL:(NSURL *)apiURL
{
    if (![_apiURL isEqualTo:apiURL]) {
        _apiURL = apiURL;

        self.isDirty = YES;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
+ (void)setFeedsVisibility:(unsigned char)visibility
{
    staticFeedsVisibility = visibility;
}

// ---------------------------------------------------------------------------------------------------------------------
+ (unsigned char)feedsVisibility
{
    return staticFeedsVisibility;
}

// ---------------------------------------------------------------------------------------------------------------------
+ (void)setAcceptSelfSignedCert:(BOOL)accept
{
    staticAcceptSelfSignedCert = accept;
}

// ---------------------------------------------------------------------------------------------------------------------
+ (BOOL)acceptSelfSignedCert
{
    return staticAcceptSelfSignedCert;
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark - implementacja DataServiceTreeItem
- (NSString *)title
{
    return self.name;
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)hasBadge
{
    return NO;
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSInteger)badgeValue
{
    return self.allFeed.unread;
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSUInteger)numberOfChildren
{
    return [self.feeds count];
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSArray *)children
{
    return self.feeds;
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)isExpandable
{
    return YES;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)commit
{
    if (self.isDirty) {
        [MicroRSSServiceTable updateTTRSSService:self];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)removeFromDB
{
    if (self.serviceID) {
        [MicroRSSServiceTable deleteTTRSSService:self];

        [[NSFileManager defaultManager] removeItemAtURL:[self iconsPath] error:nil];
    }
}

@end