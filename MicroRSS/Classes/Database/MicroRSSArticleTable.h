//
// Created by Grzesiek on 10.07.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>

@class TinyTinyRSSFeed;
@class TinyTinyRSS;

@interface MicroRSSArticleTable : NSObject

/**
* Aktualizuje artykuły w bazie danych
*/
+ (void)updateTTRSSArticles:(NSArray *)articles forFeed:(TinyTinyRSSFeed *)feed append:(BOOL)appendArticles;

/**
* Dodaje nowe artykuły kasując dokładnie tyle samo najstarszych.
*/
+ (void)addNewTTRSSArticles:(NSArray *)articles forFeed:(TinyTinyRSSFeed *)feed;

/**
* Pobiera z bazy artykuły dla danego feed'a
*/
+ (NSMutableArray *)fetchTTRSSArticlesForFeed:(TinyTinyRSSFeed *)feed andService:(TinyTinyRSS *)service;

/**
* Zwraca najwyższą dostępną wartość identyfikatora artykułu dla danego feed'a
*/
+ (int)topArticleIDForFeedID:(NSInteger)feedDBID;

/**
* Oznacza dany artykuł jako przeczytany
*/
+ (void)markArticle:(NSInteger)articleID forService:(NSInteger)serviceID asUnread:(BOOL)unread;

/**
* Oznacza dany artykuł gwiazdką
*/
+ (void)markArticle:(NSInteger)articleID forService:(NSInteger)serviceID asStarred:(BOOL)starred;

/**
* Oznacza dany artykuł jako opublikowany
*/
+ (void)markArticle:(NSInteger)articleID forService:(NSInteger)serviceID asPublished:(BOOL)published;

/**
* Zapisuje maksymalną wartość ID artykułu
*/
+ (void)saveArticleMaxID:(NSInteger)articleID forFeed:(NSInteger)feedDBID;

/**
* Pobiera maksymalną wartość artykułu
*/
+ (NSInteger)articleMaxID:(NSInteger)feedDBID;
@end