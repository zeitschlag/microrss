//
// Created by Grzesiek on 17.06.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import "MicroRSSInitialMigration.h"


@implementation MicroRSSInitialMigration

// ---------------------------------------------------------------------------------------------------------------------
- (void)up
{
    // tabela z dostępnymi serwisami
    [self.db executeUpdate:@"CREATE TABLE default_service ("
            "\"id\" integer NOT NULL PRIMARY KEY AUTOINCREMENT,"
            "\"name\" text(255,0) NOT NULL, "
            "\"login\" TEXT(255,0) NOT NULL, "
            "\"api_url\" TEXT(255,0) NOT NULL, "
            "\"service_type\" integer NOT NULL DEFAULT 0"
            ");"
    ];

    // tabela z kategoriami
    [self.db executeUpdate:@"CREATE TABLE default_feed ("
            "\"id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
            "\"service_id\" INTEGER NOT NULL,"
            "\"feed_id\" INTEGER NOT NULL,"
            "\"category_id\" INTEGER,"
            "\"name\" TEXT NOT NULL,"
            "\"unread\" INTEGER NOT NULL DEFAULT 0,"
            "\"last_update\" INTEGER NOT NULL DEFAULT 0,"
            "\"last_server_update\" INTEGER NOT NULL DEFAULT -1,"
            "\"has_feed_icon\" INTEGER(1,0) NOT NULL DEFAULT 0,"
            "\"is_category\" INTEGER(1,0) NOT NULL DEFAULT 0,"
            "\"order_id\" INTEGER NOT NULL DEFAULT 0,"
            "CONSTRAINT \"default_feed_service_id_default_service_id\" FOREIGN KEY (\"service_id\") REFERENCES \"default_service\" (\"id\") ON DELETE CASCADE ON UPDATE CASCADE"
            ");"
    ];

    // klucz na relację
    [self.db executeUpdate:@"CREATE INDEX \"default_feed_service_id\" ON default_feed (\"service_id\");"];

    // tabela z artykułami
    [self.db executeUpdate:@"CREATE TABLE default_article ("
            "\"id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
            "\"feed_db_id\" INTEGER,"
            "\"service_id\" INTEGER NOT NULL,"
            "\"feed_id\" INTEGER NOT NULL,"
            "\"article_id\" INTEGER NOT NULL,"
            "\"title\" TEXT NOT NULL,"
            "\"unread\" INTEGER(1,0) NOT NULL DEFAULT 0,"
            "\"starred\" INTEGER(1,0) NOT NULL DEFAULT 0,"
            "\"published\" INTEGER(1,0) NOT NULL DEFAULT 0,"
            "\"link\" TEXT NOT NULL,"
            "\"excerpt\" TEXT NOT NULL DEFAULT '',"
            "\"content\" TEXT NOT NULL DEFAULT '',"
            "\"updated\" INTEGER NOT NULL DEFAULT 0,"
            "CONSTRAINT \"default_article_feed_db_id_default_feed_id\" FOREIGN KEY (\"feed_db_id\") REFERENCES \"default_feed\" (\"id\") ON DELETE CASCADE ON UPDATE CASCADE,"
            "CONSTRAINT \"default_article_service_id_default_service_id\" FOREIGN KEY (\"service_id\") REFERENCES \"default_service\" (\"id\") ON DELETE CASCADE ON UPDATE CASCADE"
            ");"];

    // klucze
    [self.db executeUpdate:@"CREATE INDEX \"default_article_feed_db_id\" ON default_article (\"feed_db_id\");"];
    [self.db executeUpdate:@"CREATE INDEX \"default_article_service_id_article_id\" ON default_article (\"service_id\", \"article_id\");"];
}

@end