//
// Created by Grzesiek on 14.11.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import "MicroRSSMigrationV2.h"


@implementation MicroRSSMigrationV2

// ---------------------------------------------------------------------------------------------------------------------
- (void)up
{
    [self.db executeUpdate:@"ALTER TABLE default_feed ADD COLUMN article_sort_order INTEGER NOT NULL DEFAULT 0"];
    [self.db executeUpdate:@"ALTER TABLE default_feed ADD COLUMN article_visibility INTEGER NOT NULL DEFAULT 0"];

    // tabela z informacją o najnowszym numerze ID feed'a
    [self.db executeUpdate:@"CREATE TABLE default_article_max_id ("
            "\"id\" INTEGER NOT NULL PRIMARY KEY,"
            "\"value\" INTEGER NOT NULL,"
            "CONSTRAINT \"default_article_max_id_id_default_feed_id\" FOREIGN KEY (\"id\") REFERENCES \"default_feed\" (\"id\") ON DELETE CASCADE ON UPDATE CASCADE"
            ");"
    ];

    FMResultSet *countResultSet = [self.db executeQuery:@"SELECT feed_db_id, MAX(article_id) AS max_article FROM default_article WHERE "
            "feed_db_id IN (SELECT id FROM default_feed WHERE feed_id = -4) GROUP BY feed_db_id"];
    while ([countResultSet next]) {
        int max    = [countResultSet intForColumn:@"max_article"];
        int feedID = [countResultSet intForColumn:@"feed_db_id"];

        [self.db executeUpdate:@"INSERT INTO default_article_max_id VALUES (?, ?)", [NSNumber numberWithInt:feedID], [NSNumber numberWithInt:max]];
    }
}

@end