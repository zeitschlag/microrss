//
// Created by Grzesiek on 22.06.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>

@class FMDatabase;
@protocol DataServiceDelegate;
@class TinyTinyRSS;


@interface MicroRSSServiceTable : NSObject

/**
* Zwraca tablicę ze wszystkimi serwisamy dostępnymi w bazie inicjalizując je odrazu przy użyciu delegata przekazanego w parametrze.
*
* @param db baza danych na której zostaną wykonane zapytania
* @param delegate delegat użyty do inicjalizacji
* @return tablica z listą obiektów TinyTinyRSS
*/
+ (NSMutableArray *)fetchAllForDatabase:(FMDatabase *)db withDelegate:(id <DataServiceDelegate>)delegate;

/**
* Zapisuje dane serwisu do bazy danych.
*
* @param service zapisywany serwis
*/
+ (void)updateTTRSSService:(TinyTinyRSS *)service;

/**
* Kasuje dane z bazy.
*
* @param rss serwis do usunięcia
*/
+ (void)deleteTTRSSService:(TinyTinyRSS *)rss;
@end