//
// Created by Grzesiek on 22.06.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import "MicroRSSServiceTable.h"
#import "FMDatabase.h"
#import "TinyTinyRSS.h"
#import "DataServiceDelegate.h"
#import "MicroRSSDatabase.h"
#import "FMDatabaseQueue.h"
#import "SSKeychain.h"


@implementation MicroRSSServiceTable

// ---------------------------------------------------------------------------------------------------------------------
+ (NSMutableArray *)fetchAllForDatabase:(FMDatabase *)db withDelegate:(id<DataServiceDelegate>)delegate
{
    FMResultSet *resultSet = [db executeQuery:@"SELECT * FROM default_service"];

    NSMutableArray *resultArray = [[NSMutableArray alloc] init];

    while ([resultSet next]) {
        NSString *accountName = [[NSString alloc] initWithFormat:@"s%d", [resultSet intForColumn:@"id"]];
        NSString *password    = [SSKeychain passwordForService:@"eu.mobilegb.MicroRSS" account:accountName];
        if (!password) {
            password = @""; // na wszelki wypadek aby nie było szansy, że będzie nil
        }

        NSURL *apiURL = [[NSURL alloc] initWithString:[resultSet stringForColumn:@"api_url"]];

        TinyTinyRSS *service = [[TinyTinyRSS alloc] initWithApiURL:apiURL
                                                          delegate:delegate
                                                             login:[resultSet stringForColumn:@"login"]
                                                          password:password
                                                              name:[resultSet stringForColumn:@"name"]];

        service.serviceID = [resultSet intForColumn:@"id"];

        [resultArray addObject:service];
    }

    return resultArray;
}

// ---------------------------------------------------------------------------------------------------------------------
+ (void)updateTTRSSService:(TinyTinyRSS *)service
{
    FMDatabase *db = [FMDatabase databaseWithPath:[MicroRSSDatabase databasePathString]];

    if (![db open]) {
        return;
    }

    if (service.serviceID) {
        NSNumber *idNumber = [[NSNumber alloc] initWithInteger:service.serviceID];

        [db executeUpdate:@"UPDATE default_service SET name = ?, login = ?, api_url = ? WHERE id = ?",
                        service.name,
                        service.login,
                        [service.apiURL absoluteString],
                        idNumber];
    } else {
        [db executeUpdate:@"INSERT INTO default_service (name, login, api_url, service_type) VALUES (?,?,?,?)",
                        service.name,
                        service.login,
                        [service.apiURL absoluteString],
                        @0];

        service.serviceID = (NSInteger)[db lastInsertRowId];
    }

    NSString *accountName = [[NSString alloc] initWithFormat:@"s%ld", service.serviceID];
    [SSKeychain setPassword:service.password forService:@"eu.mobilegb.MicroRSS" account:accountName];
}

// ---------------------------------------------------------------------------------------------------------------------
+ (void)deleteTTRSSService:(TinyTinyRSS *)rss
{
    FMDatabaseQueue *queue = [[FMDatabaseQueue alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    [queue inDatabase:^(FMDatabase *db) {
        NSString *accountName = [[NSString alloc] initWithFormat:@"s%ld", rss.serviceID];
        [SSKeychain deletePasswordForService:@"eu.mobilegb.MicroRSS" account:accountName];

        NSNumber *idNumber = [[NSNumber alloc] initWithInteger:rss.serviceID];

        [db executeUpdate:@"DELETE FROM default_article WHERE service_id = ?", idNumber];
        [db executeUpdate:@"DELETE FROM default_article_max_id WHERE id IN (SELECT id FROM default_feed WHERE service_id = ?)", idNumber];
        [db executeUpdate:@"DELETE FROM default_feed WHERE service_id = ?", idNumber];
        [db executeUpdate:@"DELETE FROM default_service WHERE id = ?", idNumber];
    }];
}

@end