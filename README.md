MicroRSS is an unofficial Tiny Tiny RSS client. It requires access to Tiny Tiny RSS server. TTRSS application can be downloaded for free from [http://tt-rss.org](http://tt-rss.org).

MicroRSS is licensed under General Public Licence version 3.